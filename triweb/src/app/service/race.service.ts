import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { BaseService } from './base.service';
import { RaceResult } from '../dto/race-result';
import { Race } from '../dto/race';
import { Name } from '../dto/name';
import { environment } from '../../environments/environment';
import * as _ from 'lodash';

@Injectable()
export class RaceService extends BaseService {

    constructor(private http: Http) { super(); }

    get_record(id: string): Observable<any> {
        let req_url: string = environment.race_service_url + '/record/' + id; 
        return this.http.get(req_url)
            .map((res:Response) => res.json().result as any)
            .catch(this.handleError)
    }

    search_athlete(term: string, limit: number = 10): Observable<Name[]> {
        if(term == null || term.trim() == '') {
            return Observable.of([]);
        }

        let req_url: string = environment.race_service_url + '/athlete/' + term;
        if(limit) {
            req_url = req_url + '/' + limit;
        } 

        return this.http.get(req_url)
            .map((res:Response) => res.json().result as Name[])
            .catch(this.handleError);            
    }

    search_result(gender: string, first: string, last: string, country: string): Observable<RaceResult[]> {
        let req_url: string = environment.race_service_url + '/search/' + gender + '/' + first + '/' + last + '/' + country; 
        return this.http.get(req_url)
            .map((res:Response) => res.json().result as RaceResult[])
            .catch(this.handleError);            
    }

    get_race(year: number = null): Observable<Race[]> {
        let req_url = environment.race_service_url + '/race/' + (year ? '/' + year : '') 
        return this.http.get(req_url)
            .map((res:Response) => res.json().result as Race[])
            .catch(this.handleError)
    }

    get_race_result(race_type:string, race_region: string, race_code: string, race_year: number): Observable<RaceResult[]> {
        let req_url = environment.race_service_url + '/race_result/' + race_type + '/' + race_region + '/' + race_code + '/' + race_year;
        return this.http.get(req_url)
            .map((res:Response) => res.json().result as RaceResult[])
            .catch(this.handleError)
    }
}
