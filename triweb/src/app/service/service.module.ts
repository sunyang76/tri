import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DtoModule } from '../dto/dto.module';
import { RaceService } from './race.service';
@NgModule({
    imports: [
        CommonModule,
        DtoModule
    ],
    declarations: []
})
export class ServiceModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: ServiceModule,
            providers: [RaceService]
        }
    }
}
