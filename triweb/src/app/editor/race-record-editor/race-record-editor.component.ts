import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { RaceService } from '../../service/race.service';
import { Race } from '../../dto/race';
import { RaceResult } from '../../dto/race-result';

@Component({
    selector: 'app-race-record-editor',
    templateUrl: './race-record-editor.component.html',
    styleUrls: ['./race-record-editor.component.css']
})
export class RaceRecordEditorComponent implements OnInit {

    private errorMessage: string;
    private record: any;

    constructor(private activeModal: NgbActiveModal, private raceService: RaceService) { }

    public load(id: string): void {
        console.log(id);
        this.raceService.get_record(id)
            .subscribe(record => this.record = record,
            error => this.errorMessage = <any>error)
    }

    ngOnInit() {
    }

}
