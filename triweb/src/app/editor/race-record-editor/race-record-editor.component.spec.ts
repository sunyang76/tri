import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceRecordEditorComponent } from './race-record-editor.component';

describe('RaceRecordEditorComponent', () => {
  let component: RaceRecordEditorComponent;
  let fixture: ComponentFixture<RaceRecordEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RaceRecordEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceRecordEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
