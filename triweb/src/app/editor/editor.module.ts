import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RaceRecordEditorComponent } from './race-record-editor/race-record-editor.component';
import { ServiceModule } from '../service/service.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ServiceModule.forRoot()
  ],
  declarations: [RaceRecordEditorComponent]
})
export class EditorModule { }
