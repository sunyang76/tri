import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DtoModule } from './dto/dto.module';
import { ServiceModule } from './service/service.module';
import { SearchModule } from './search/search.module';
import { ShareModule } from './share/share.module';
import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent        
    ],
    imports: [
        BrowserModule,
        FormsModule,              
        HttpModule,        
        AppRoutingModule,
        ShareModule,
        SearchModule,        
        ServiceModule.forRoot(),
        NgbModule.forRoot()
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
