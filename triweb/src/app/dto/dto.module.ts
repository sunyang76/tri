import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RaceResult } from './race-result';
@NgModule({
    imports: [
        CommonModule
    ],
    declarations: []
})
export class DtoModule { }
