export interface RaceResult {
    id: string;
    year: number;
    type: string;
    region: string;
    code: string;
    bib: string;
    gender: string;
    first: string;     
    last: string;
    profession: string;
    country: string;
    state: string;
    city: string;       
    division: string;
    overall: number;          
    swim: number;
    bike: number;
    run: string;
    status: string;
}
