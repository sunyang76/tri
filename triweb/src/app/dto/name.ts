export interface Name {
    first: string;
    last: string;
    gender: string;
    country: string
}
