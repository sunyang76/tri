export interface Race {
    year: number;
    type: string;
    region: string;
    code: string;
    name: string;
}