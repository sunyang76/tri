import { Component, OnInit } from '@angular/core';
import { GridOptions, RowNode } from "ag-grid";
import { RaceService } from '../../service/race.service';
import { NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { DurationComponent } from '../../share/ag-grid-extension/duration.component';
import { GenderComponent } from '../../share/ag-grid-extension/gender.component';
import { Name } from '../../dto/name';
import { RaceResult } from '../../dto/race-result';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'app-athlete',
    templateUrl: './athlete.component.html',
    styleUrls: ['./athlete.component.css']
})
export class AthleteComponent implements OnInit {
    private errorMessage: string;
    private resultGridOptions: GridOptions;
    public athleteSearchText: string;
    selected: Name;
    searching = false;
    searchFailed = false;
    constructor(private raceService: RaceService) { }

    search = (text$: Observable<string>) =>
        text$
            .debounceTime(300)
            .distinctUntilChanged()
            .do(() => this.searching = true)
            .switchMap(term =>
                this.raceService.search_athlete(term)
                    .do(() => this.searchFailed = false)
                    .catch(() => {
                        this.searchFailed = true;
                        return Observable.of([]);
                    }))
            .do(() => this.searching = false);

    formatter = (x: Name) => x.first + ' ' + x.last;

    selectAthlete(env: NgbTypeaheadSelectItemEvent): void {
        let athlete: any = env.item;
        this.raceService.search_result(athlete.gender, athlete.first, athlete.last, athlete.country)
            .subscribe(results => this.processResult(results),
            error => this.errorMessage = <any>error)
    }

    public onAthleteFilterChanged(): void {
        this.resultGridOptions.api.setQuickFilter(this.athleteSearchText);
    }

    ngOnInit() {
        this.initGrid();
    }

    private processResult(results: RaceResult[]) {        
        this.resultGridOptions.api.setRowData(results);
    }

    private initGrid() {
        this.resultGridOptions = {
            enableSorting: true,
            enableFilter: true,
            rowSelection: 'single',
            // onRowClicked: this.onRowClicked.bind(this)
        };

        this.resultGridOptions.columnDefs = [
            {
                headerName: "Year",
                field: "year",
                width: 60,
                pinned: 'left'
            }, {
                headerName: "Type",
                field: "type",
                width: 100,
                pinned: 'left'              
            }, {
                headerName: "Region",
                field: "region",
                width: 180,
                pinned: 'left'
            },
            /*{
                headerName: "Code",
                field: "code",
                width: 250
            }, */
            {
                headerName: "Code",
                field: "code",
                width: 200,
                pinned: 'left'
            }, {
                headerName: "",
                field: "gender",
                width: 50,
                cellRendererFramework: GenderComponent,
                pinned: 'left'
            }, {
                headerName: "First",
                field: "first",
                width: 150,
                pinned: 'left'
            }, {
                headerName: "Last",
                field: "last",
                width: 150,
                pinned: 'left'
            }, {
                headerName: "Div",
                field: "division",
                width: 100,
                pinned: 'left'
            }, {
                headerName: "Overall",
                field: "overall",
                cellRendererFramework: DurationComponent,
                width: 100
            }, {
                headerName: "Swim",
                field: "swim",
                cellRendererFramework: DurationComponent,
                width: 100
            }, {
                headerName: "Bike",
                field: "bike",
                cellRendererFramework: DurationComponent,
                width: 100
            }, {
                headerName: "Run",
                field: "run",
                cellRendererFramework: DurationComponent,
                width: 100
            }, {
                headerName: "Country",
                field: "country",
                width: 100
            }, {
                headerName: "State",
                field: "state",
                width: 100
            }, {
                headerName: "City",
                field: "city",
                width: 150
            }, {
                headerName: "BIB",
                field: "bib",
                width: 100
            }, {
                headerName: "Status",
                field: "status",
                width: 100
            }, {
                headerName: "Profession",
                field: "profession",
                width: 200
            }

        ];
    }

}
