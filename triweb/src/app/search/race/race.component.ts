import { Component, OnInit, Input } from '@angular/core';
import { GridOptions, RowNode } from "ag-grid";
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { RaceService } from '../../service/race.service';
import { Race } from '../../dto/race';
import { RaceResult } from '../../dto/race-result';
import { DurationComponent } from '../../share/ag-grid-extension/duration.component';
import { GenderComponent } from '../../share/ag-grid-extension/gender.component';
import { RaceRecordEditorComponent } from '../../editor/race-record-editor/race-record-editor.component';


@Component({
    selector: 'app-race',
    templateUrl: './race.component.html',
    styleUrls: ['./race.component.css']
})
export class RaceComponent implements OnInit {
    private errorMessage: string;
    private raceGridOptions: GridOptions;
    private resultGridOptions: GridOptions;
    public raceSearchText: string;
    public athleteSearchText: string;


    constructor(private raceService: RaceService, private modalService: NgbModal) { }

    public onRaceFilterChanged(): void {
        this.raceGridOptions.api.setQuickFilter(this.raceSearchText);
    }

    public onAthleteFilterChanged(): void {
        this.resultGridOptions.api.setQuickFilter(this.athleteSearchText);
    }

    private initGrid() {
        this.raceGridOptions = {
            enableSorting: true,
            enableFilter: true,
            rowSelection: 'single',
            onRowClicked: this.onRowClicked.bind(this)
        };

        this.raceGridOptions.columnDefs = [
            {
                headerName: "Year",
                field: "year",
                width: 60
            }, {
                headerName: "Type",
                field: "type",
                width: 100
            }, {
                headerName: "Region",
                field: "region",
                width: 180
            },
            /*{
                headerName: "Code",
                field: "code",
                width: 250
            }, */
            {
                headerName: "Name",
                field: "name",
                width: 200
            },
        ];

        this.resultGridOptions = {
            enableSorting: true,
            enableFilter: true,
            rowSelection: 'single',
            onRowDoubleClicked: this.onRaceRecordDblClicked.bind(this)
        };

        this.resultGridOptions.columnDefs = [
            {
                headerName: "",
                field: "gender",
                width: 80,
                cellRendererFramework: GenderComponent,
                pinned: 'left'
            }, {
                headerName: "First",
                field: "first",
                width: 100,
                pinned: 'left'
            }, {
                headerName: "Last",
                field: "last",
                width: 100,
                pinned: 'left'
            }, {
                headerName: "Division",
                field: "division",
                width: 100,
                pinned: 'left'
            }, {
                headerName: "Overall",
                field: "overall",
                cellRendererFramework: DurationComponent,
                width: 100
            }, {
                headerName: "Swim",
                field: "swim",
                cellRendererFramework: DurationComponent,
                width: 80
            }, {
                headerName: "T1",
                field: "t1",
                cellRendererFramework: DurationComponent,
                width: 80
            }, {
                headerName: "Bike",
                field: "bike",
                cellRendererFramework: DurationComponent,
                width: 80
            }, {
                headerName: "T2",
                field: "t2",
                cellRendererFramework: DurationComponent,
                width: 80
            }, {
                headerName: "Run",
                field: "run",
                cellRendererFramework: DurationComponent,
                width: 80
            }, {
                headerName: "Country",
                field: "country",
                width: 100
            }

        ];
    }    

    private processResult(results: RaceResult[]) {
        this.resultGridOptions.api.setRowData(results);
    }

    private loadRaceResult(race: Race): void {
        this.raceService.get_race_result(race.type, race.region, race.code, race.year)
            .subscribe(results => this.processResult(results),
            error => this.errorMessage = <any>error)
    }

    private onRaceRecordDblClicked(event: any) {
        let result = event.data as RaceResult;
        let option:NgbModalOptions = {
            size: 'lg'
        };
        const modalRef = this.modalService.open(RaceRecordEditorComponent, option);
        // modalRef.componentInstance.id = result.id;
        modalRef.componentInstance.load(result.id);
    }

    private onRowClicked(event: any) {
        var race = event.data as Race;
        this.loadRaceResult(event.data as Race);
    }

    private processRace(races: Race[]): void {
        this.raceGridOptions.api.setRowData(races);
        // this.raceGridOptions.api.addEventListener('rowClicked', this.onRowClicked);
    }

    private loadRace(): void {
        this.raceService.get_race()
            .subscribe(races => this.processRace(races),
            error => this.errorMessage = <any>error)
    }

    ngOnInit() {
        this.initGrid();
        this.loadRace();
    }

}
