import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AgGridModule } from "ag-grid-angular/main";
import { AthleteComponent } from './athlete/athlete.component';
import { EditorModule} from '../editor/editor.module';
import { RaceComponent } from './race/race.component';
import { SearchRoutingModule } from './search-routing.module';
import { ServiceModule } from '../service/service.module';
import { DurationComponent } from '../share/ag-grid-extension/duration.component';
import { GenderComponent } from '../share/ag-grid-extension/gender.component';
import { RaceRecordEditorComponent} from '../editor/race-record-editor/race-record-editor.component';
@NgModule({
    imports: [
        CommonModule,        
        SearchRoutingModule,
        FormsModule,        
        ServiceModule,
        EditorModule,
        AgGridModule.withComponents([DurationComponent, GenderComponent]),
        ServiceModule.forRoot(),
        NgbModule.forRoot()
    ],
    declarations: [AthleteComponent, RaceComponent],
    entryComponents: [RaceRecordEditorComponent]
})
export class SearchModule { }
