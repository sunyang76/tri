import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AthleteComponent } from './athlete/athlete.component';
import { RaceComponent } from './race/race.component';

const searchRoutes: Routes = [
    { path: 'search/athlete', component: AthleteComponent },
    { path: 'search/race', component: RaceComponent }
];

@NgModule({
    imports: [
        RouterModule.forChild(searchRoutes)
    ],
    exports: [RouterModule]
})
export class SearchRoutingModule { }

