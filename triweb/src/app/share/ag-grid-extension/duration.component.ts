import { Component } from '@angular/core';

@Component({
    selector: 'app-duration',
    templateUrl: './duration.component.html',
    styleUrls: ['./duration.component.css']
})
export class DurationComponent {
    private duration: string;

    agInit(params: any): void {
        if (params.value) {
            var sec_num = params.value;
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            this.duration = hours + ':' + (minutes < 10 ? '0' + minutes : minutes.toString()) + ':' + (seconds < 10 ? '0' + seconds : seconds.toString());
        } else {
            this.duration = null;
        }
    }
}
