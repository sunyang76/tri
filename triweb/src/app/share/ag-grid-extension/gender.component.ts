import { Component } from '@angular/core';

@Component({
    selector: 'app-gender',
    templateUrl: './gender.component.html',
    styleUrls: ['./gender.component.css']
})
export class GenderComponent {

    private icon_class: string;
    private bib: string;
    private status: string;

    agInit(params: any): void {        
        this.bib = params.data.bib;
        this.status = params.data.status;

        if (params.value == 'F') {
            this.icon_class = 'fa fa-venus';
        } else {
            this.icon_class = 'fa fa-mars';
        }


    }

}
