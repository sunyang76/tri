import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TopNavComponent } from './top-nav/top-nav.component';
import { RaceResultComponent } from './race-result/race-result.component';
import { DurationComponent } from './ag-grid-extension/duration.component';
import { GenderComponent } from './ag-grid-extension/gender.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [TopNavComponent, RaceResultComponent, DurationComponent, GenderComponent],
    exports: [TopNavComponent, RaceResultComponent]
})
export class ShareModule { }
