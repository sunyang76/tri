import { TriwebPage } from './app.po';

describe('triweb App', () => {
  let page: TriwebPage;

  beforeEach(() => {
    page = new TriwebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
