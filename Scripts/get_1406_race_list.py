from urllib.request import urlopen
import re

response = urlopen('http://www.ironman.com/events/triathlon-races.aspx?d=ironman')
html = response.read().decode('utf-8', 'ignore')
race_link = re.findall("\"http://www.ironman.com/triathlon/events/(\w+)/ironman/(.+).aspx\" class=\"eventDetails\"", html)
for r in race_link:
    print("'" + r[1].replace('-', '') + "_140.6': Race('" + r[0] + "','" + r[1]+ "', 'ironman', '" + r[1].replace('-', '') + "'),")
