import sys
import os
from import_race import import_file

if __name__ == "__main__":
    root = sys.argv[1]
    for d in os.walk(root, topdown=True):
        for f in d[2]:
            filename = os.path.join(d[0], f) 
            if filename[-4:] == '.csv':            	
            	import_file(filename)