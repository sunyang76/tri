from urllib.request import urlopen
import re

response = urlopen('http://www.ironman.com/events/triathlon-races.aspx?d=ironman+70.3')
html = response.read().decode('utf-8', 'ignore')
race_link = re.findall("\"http://www.ironman.com/triathlon/events/(\w+)/ironman-70.3/(.+).aspx\" class=\"eventDetails\"", html)
for r in race_link:
    print("'" + r[1].replace('-', '') + "_70.3': Race('" + r[0] + "','" + r[1]+ "', 'ironman-70.3', '" + r[1].replace('-', '') + "70.3'),")
