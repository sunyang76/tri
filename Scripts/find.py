import sys
import os
from import_race import import_file

if __name__ == "__main__":
    root = sys.argv[1]
    print(root)
    for d in os.walk(root, topdown=True):
        for f in d[2]:
            filename = d[0] + '/' + f 
            if 'frankfurt' in filename:            	
            	import_file(filename)