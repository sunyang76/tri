import sys
import logging
import codecs
import re
from urllib.request import urlopen
import html as html_util
from enum import Enum
from bs4 import BeautifulSoup
from io import StringIO
import csv
import os
from multiprocessing.pool import ThreadPool


class CsvWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f):        
        self.writer = csv.writer(f, delimiter='|')

    def writerow(self, row):
        self.writer.writerow(row)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class Race:
    def __init__(self, region, name, race_type, result_race_name):
        self.region = region
        self.name = name
        self.race_type = race_type
        self.result_race_name = result_race_name
        self.url = 'http://www.ironman.com/triathlon/events/{0}/{1}/{2}/results.aspx'.format(region, race_type, name)
        self.result = []
        self.header = []

    def __str__(self):
        return "Name: {0}, Type: {1}, Url: {2}".format(self.name, self.race_type, self.url)

    def get(self, years, as_of_year):
        self.__get_years__(years, as_of_year)
        # result = {
        #     "name": "{0}_{1}_{2}_{3}.csv".format(self.region, self.name, self.race_type, self.race_date),
        #     "header": self.header,
        #     "rows": self.result
        # }

        # return result

    def __get_years__(self, years, as_of_year):        
        response = urlopen(self.url)
        html = response.read().decode('utf-8', 'ignore')       
        # self.m_years = re.findall("<li><a href=\"(" + self.url + "\?rd=(\d*))\"( class=\"active\")?><span class=\"ss-icon\">&#x25C5;</span>(\d\d/\d\d/\d\d\d\d) Results</a></li>", html)          
        self.m_years = re.findall("<li><a href=\"(" + self.url + "\?rd=(\d*))\"( class=\"active\")?><span class=\"ss-icon\">", html)                  
        
        if len(self.m_years) == 0:
            self.m_years = [('http://www.ironman.com/triathlon/events/emea/ironman-70.3/jonkoping/results.aspx?rd=20160710', '20160710', ' class="active"')]    # this is hard code for individual race

        print(self.m_years)

        for y in self.m_years:            
            int_y = int(y[1]) // 10000                 
            if len(years) == 0 or int_y in years:                
                result = {
                    "name": "{0}_{1}_{2}_{3}_{4}.csv".format(self.region, self.name, self.race_type, y[1], as_of_year),
                    "race_date": y[1],
                    "header": [],
                    "row": []
                }
                
                self.__get_year__(y, 'M', result, as_of_year)                            
                self.__get_year__(y, 'F', result, as_of_year)                

                if len(result["row"]) > 0:
                    
                    
                    race_folder = self.name.replace('-', '')
                    if self.race_type =='ironman':
                        race_folder += '_140.6'
                    else:
                        race_folder += '_70.3'

                    race_folder = os.path.join(root, race_folder)

                    if not os.path.exists(race_folder):
                        os.makedirs(race_folder)

                    race_file = os.path.join(race_folder, result["name"])
                    print("save to " + race_file)
                    with open(race_file, 'w', newline='', encoding='utf-8') as csvfile:
                        writer = CsvWriter(csvfile)
                        writer.writerow(result["header"])        
                        writer.writerows(result["row"]);
           

    def __get_year__(self, year_race_info, gender, result, as_of_year):
        # print(year_race_info)
        print('get {0} atheltes for year {1}'.format(gender, year_race_info[1]))                
        response = urlopen(year_race_info[0] + '&sex=' + gender)
        html = response.read().decode('utf-8', 'ignore')
        page = 1
        try:
            page = int(re.findall("<a title=\"Go to page (\d+)\"", html)[-1])                
        except:
            print('single page')

        for p in range(1, page+1):           
            print('get page {0} of {1}'.format(p, page))
            page_url = '{0}?p={1}&rd={2}&sex={3}'.format(self.url, p, year_race_info[1], gender)
            # self.race_date = year_race_info[1]
            self.__get_page__(page_url, year_race_info[1], gender, result, as_of_year)            
            # break

    def __get_page__(self, page_url, race_date, gender, result, as_of_year):        
        response = urlopen(page_url)
        # print(page_url)
        html = response.read().decode('utf-8', 'ignore')        
        race_name = self.result_race_name        
        
        # print(race_name)    
        athlinks = re.findall("<a href=\"(\?rd=" + race_date + "&amp;race=" + race_name + "&amp;bidid=(\d+)&amp;detail=1)\" class=\"athlete\">(.+)</a>", html)        
        # athlinks = re.findall("<a href=\"\?rd=" + race_date  + "&amp;race=....", html)        
        # print(athlinks)    
        inputs = [[html_util.unescape('{0}{1}'.format(self.url, ath[0])), ath[2], gender ] for ath in athlinks]
        # print(inputs[0])
        data = pool.map(self.__get_record__, inputs)

        for rec in data:            
            self.__to_flat__(rec, result, as_of_year)

        print("parsed records : " + str(len(result["row"])))
    
    def __get_record__(self, params):
        
        record_link = params[0]
        full_name = params[1]
        gender = params[2]
        # print(full_name)
        response = urlopen(record_link)
        html = response.read().decode('utf-8', 'ignore')
        soup = BeautifulSoup(html, 'html.parser') 
        header_data = soup.find_all('header')        
        tables = soup.find_all('table')

        t_general_info = tables[0]
        t_summary = tables[1]
        t_swim_detail = tables[2]
        t_bike_detail = tables[3]
        t_run_detail = tables[4]
        t_transition_detail = tables[5]

        result = {}
        name_parts = full_name.split(',')
        name = {            
            'gender': gender,
            'last': name_parts[0].strip()
        }

        if(len(name_parts) > 1):
            name['first'] = name_parts[1].strip()

        result['name'] = name
        result['header'] = self.__get_header_info(header_data[2])       #use 3 for texas
        result['general'] = self.__get_general_info__(t_general_info)        
        result['summary'] = self.__get_summary_info__(t_summary)
        result['swim'] = self.__get_swim_info__(t_swim_detail)
        result['bike'] = self.__get_bike_info__(t_bike_detail)
        result['run'] = self.__get_run_info__(t_run_detail)
        result['transition'] = self.__get_transition_info__(t_transition_detail)        
        return result;
    
    def __get_header_info(self, data):
        result = {}
        # result['name'] = data.find_all('h1')[0].text
        result['overall_rank'] = data.find_all('div', {'id': 'div-rank'})[0].text[14:]
        if re.match("\d+", result['overall_rank']):
            result['status'] = 'FINISH'
        else:
            result['status'] = result['overall_rank']
            result['overall_rank'] = ''

        result['div_rank'] = data.find_all('div', {'id': 'rank'})[0].text[10:]
        if not re.match("\d+", result['div_rank']):
            result['div_rank'] = ''

        result['gender_rank'] = data.find_all('div', {'id': 'gen-rank'})[0].text[13:]
        if not re.match("\d+", result['gender_rank']):
            result['gender_rank'] = ''

        return result

    def __get_general_info__(self, data):
        row = data.find_all('tr')
        result = {}
        for r in row:
            tds = r.find_all('td')
            if tds[0].text == 'BIB':
                result['bib'] = tds[1].text
            if tds[0].text == 'Division':
                result['division'] = tds[1].text
            if tds[0].text == 'Age':
                result['age'] = tds[1].text
                if not re.match("\d+", result['age']):
                    result['age'] = ''
            if tds[0].text == 'State':
                result['state'] = tds[1].text
            if tds[0].text == 'Country':
                result['country'] = tds[1].text
            if tds[0].text == 'Profession':              
                result['profession'] = tds[1].text
            if tds[0].text == 'Points':
                result['points'] = tds[1].text 
                if not re.match("\d+", result['points']):
                    result['points'] = ''
        return result

    def __get_summary_info__(self, data):
        row = data.find_all('tr')
        result = {}
        for r in row:
            tds = r.find_all('td')
            #print(tds[0])
            if tds[0].text == 'Swim':
                result['swim'] = tds[1].text
                if not re.match("\d+:\d+:\d+", result['swim']):
                    result['swim'] = ''
            if tds[0].text == 'Bike':
                result['bike'] = tds[1].text
                if not re.match("\d+:\d+:\d+", result['bike']):
                    result['bike'] = ''
            if tds[0].text == 'Run':
                result['run'] = tds[1].text
                if not re.match("\d+:\d+:\d+", result['run']):
                    result['run'] = ''
            if tds[0].text == 'Overall':
                result['overall'] = tds[1].text
                if not re.match("\d+:\d+:\d+", result['overall']):
                    result['overall'] = ''            
        
        return result

    def __get_swim_info__(self, data):
        col = data.find_all('tr')[1].find_all('td')
        result = {}
        result['distance'] = col[1].text        
        result['split_time'] = col[2].text
        if not re.match("\d+:\d+:\d+", result['split_time']):
            result['split_time'] = ''
        result['race_time'] = col[3].text
        if not re.match("\d+:\d+:\d+", result['race_time']):
            result['race_time'] = ''
        result['pace'] = col[4].text
        result['division_rank'] = col[5].text
        if not re.match("\d+", result['division_rank']):
            result['division_rank'] = ''
        result['gender_rank'] = col[6].text
        if not re.match("\d+", result['gender_rank']):
            result['gender_rank'] = ''
        result['overall_rank'] = col[7].text
        if not re.match("\d+", result['overall_rank']):
            result['overall_rank'] = ''
        return result

    def __get_bike_info__(self, data):
        col = data.find_all('tr')[1].find_all('td')
        result = {}
        result['distance'] = col[1].text        
        result['split_time'] = col[2].text
        if not re.match("\d+:\d+:\d+", result['split_time']):
            result['split_time'] = ''
        result['race_time'] = col[3].text
        if not re.match("\d+:\d+:\d+", result['race_time']):
            result['race_time'] = ''
        result['pace'] = col[4].text
        result['division_rank'] = col[5].text
        if not re.match("\d+", result['division_rank']):
            result['division_rank'] = ''
        result['gender_rank'] = col[6].text
        if not re.match("\d+", result['gender_rank']):
            result['gender_rank'] = ''
        result['overall_rank'] = col[7].text
        if not re.match("\d+", result['overall_rank']):
            result['overall_rank'] = ''
        return result

    def __get_run_info__(self, data):
        col = data.find_all('tr')[1].find_all('td')
        result = {}
        result['distance'] = col[1].text        
        result['split_time'] = col[2].text
        if not re.match("\d+:\d+:\d+", result['split_time']):
            result['split_time'] = ''
        result['race_time'] = col[3].text
        if not re.match("\d+:\d+:\d+", result['race_time']):
            result['race_time'] = ''
        result['pace'] = col[4].text
        result['division_rank'] = col[5].text
        if not re.match("\d+", result['division_rank']):
            result['division_rank'] = ''
        result['gender_rank'] = col[6].text
        if not re.match("\d+", result['gender_rank']):
            result['gender_rank'] = ''
        result['overall_rank'] = col[7].text
        if not re.match("\d+", result['overall_rank']):
            result['overall_rank'] = ''
        return result

    def __get_transition_info__(self, data):
        row = data.find_all('tr')
        result = {}
        result['t1'] = row[0].find_all('td')[1].text
        if not re.match("\d+:\d+:\d+", result['t1']):
            result['t1'] = ''
        result['t2'] = row[1].find_all('td')[1].text
        if not re.match("\d+:\d+:\d+", result['t2']):
            result['t2'] = ''
        return result

    def __to_flat__(self, record, result, as_of_year):
        l_name = []
        l_value = []
        for k in record.keys():        
            l_name = l_name + [ k + '_' + n for n in record[k].keys()]                
            l_value = l_value + [v for v in record[k].values()]            
        if len(result["header"]) == 0:
            result["header"] = ['region', 'race', 'type', 'date', 'as_of_year'] + l_name
        
        result["row"].append([self.region, self.name, self.race_type, result["race_date"], as_of_year] + l_value)    
        

# race_list = [
#     # Race('americas','florida', 'ironman-70.3')
#     Race('americas', "wisconsin", 'ironman')
# ]

race_dict = {
    # 'northcarolina_140.6': Race('americas','north-carolina', 'ironman', 'northcarolina'),
    # 'loscabos_140.6': Race('americas','los-cabos', 'ironman', 'loscabos'),
    # 'florida_140.6': Race('americas','florida', 'ironman', 'florida'),
    # 'malaysia_140.6': Race('asiapac','malaysia', 'ironman', 'malaysia'),
    # 'arizona_140.6': Race('americas','arizona', 'ironman', 'arizona'),
    # 'fortaleza_140.6': Race('americas','fortaleza', 'ironman', 'fortaleza'),
    # 'cozumel_140.6': Race('americas','cozumel', 'ironman', 'cozumel'),
    # 'westernaustralia_140.6': Race('asiapac','western-australia', 'ironman', 'westernaustralia'),
    # 'newzealand_140.6': Race('asiapac','new-zealand', 'ironman', 'newzealand'),
    # 'southafrica_140.6': Race('emea','south-africa', 'ironman', 'southafrica'),
    # 'texas_140.6': Race('americas','texas', 'ironman', 'texas'),
    # 'australia_140.6': Race('asiapac','australia', 'ironman', 'australia'),
    # 'lanzarote_140.6': Race('emea','lanzarote', 'ironman', 'lanzarote'),
    # 'brazil_140.6': Race('americas','brazil', 'ironman', 'brazil'),
    # 'boulder_140.6': Race('americas','boulder', 'ironman', 'boulder'),
    # 'cairns_140.6': Race('asiapac','cairns', 'ironman', 'cairns'),
    # 'france_140.6': Race('emea','france', 'ironman', 'france'),
    # 'austria_140.6': Race('emea','austria', 'ironman', 'austria'),
    # 'frankfurt_140.6': Race('emea','frankfurt', 'ironman', 'germany'),
    # 'uk_140.6': Race('emea','uk', 'ironman', 'uk'),
    # 'lakeplacid_140.6': Race('americas','lake-placid', 'ironman', 'lakeplacid'),
    # 'santarosa_140.6': Race('americas','santa-rosa', 'ironman', 'santarosa'),
    # 'switzerland_140.6': Race('emea','switzerland', 'ironman', 'switzerland'),
    # 'canada_140.6': Race('americas','canada', 'ironman', 'canada'),
    # 'maastricht_140.6': Race('emea','maastricht', 'ironman', 'maastricht'),
    # 'hamburg_140.6': Race('emea','hamburg', 'ironman', 'hamburg'),
    # 'kalmar_140.6': Race('emea','kalmar', 'ironman', 'kalmar'),
    # 'copenhagen_140.6': Race('emea','copenhagen', 'ironman', 'copenhagen'),
    # 'monttremblant_140.6': Race('americas','mont-tremblant', 'ironman', 'monttremblant'),
    # 'vichy_140.6': Race('emea','vichy', 'ironman', 'vichy'),
    # 'coeurdalene_140.6': Race('americas','coeur-dalene', 'ironman', 'coeurdalene'),
    # 'wisconsin_140.6': Race('americas','wisconsin', 'ironman', 'wisconsin'),
    # 'wales_140.6': Race('emea','wales', 'ironman', 'wales'),
    # 'chattanooga_140.6': Race('americas','chattanooga', 'ironman', 'chattanooga'),
    # 'mallorca_140.6': Race('emea','mallorca', 'ironman', 'mallorca'),
    # 'barcelona_140.6': Race('emea','barcelona', 'ironman', 'barcelona'),
    # 'taiwan_140.6': Race('asiapac','taiwan', 'ironman', 'taiwan'),
    # 'maryland_140.6': Race('americas','maryland', 'ironman', 'maryland'),
    # 'worldchampionship_140.6': Race('americas','world-championship', 'ironman', 'worldchampionship'),
    # 'louisville_140.6': Race('americas','louisville', 'ironman', 'louisville'),

    'worldchampionship_140.6': Race('americas','world-championship', 'ironman', 'worldchampionship'),
    'wisconsin_140.6': Race('americas', 'wisconsin', 'ironman', 'wisconsin'),
    'canada_140.6': Race('americas', 'canada', 'ironman', 'canada'),
    'tremblant_140.6': Race('americas', 'mont-tremblant', 'ironman', 'monttremblant'),
    'florida_140.6': Race('americas', 'florida', 'ironman', 'florida'),
    'arizona_140.6': Race('americas', 'arizona', 'ironman', 'arizona'),
    'boulder_140.6': Race('americas', 'boulder', 'ironman', 'boulder'),
    'lakeplacid_140.6': Race('americas', 'lake-placid', 'ironman', 'lakeplacid'),
    'maryland_140.6': Race('americas', 'maryland', 'ironman', 'maryland'),
    'louisville_140.6': Race('americas', 'louisville', 'ironman', 'louisville'),
    "chattanooga_140.6":  Race('americas', 'chattanooga', 'ironman', 'chattanooga'),
    "texas_140.6":  Race('americas', 'texas', 'ironman', 'texas'),    
    "coeurdalene_140.6":  Race('americas', 'coeur-dalene', 'ironman', 'coeurdalene'),    
    "cozumel_140.6":  Race('americas', 'cozumel', 'ironman', 'cozumel'),        
    "loscabos_140.6":  Race('americas', 'los-cabos', 'ironman', 'loscabos'),        
    "fortaleza_140.6":  Race('americas', 'fortaleza', 'ironman', 'fortaleza'),        
    "brazil_140.6":  Race('americas', 'brazil', 'ironman', 'brazil'),

    "lanzarote_140.6":  Race('emea', 'lanzarote', 'ironman', 'lanzarote'),
    "kalmar_140.6":  Race('emea', 'kalmar', 'ironman', 'kalmar'),
    "copenhagen_140.6":  Race('emea', 'copenhagen', 'ironman', 'copenhagen'),
    "uk_140.6":  Race('emea', 'uk', 'ironman', 'uk'),
    "wales_140.6":  Race('emea', 'wales', 'ironman', 'wales'), 
    "weymouth_140.6":  Race('emea', 'weymouth', 'ironman', 'weymouth'),
    "frankfurt_140.6":  Race('emea', 'frankfurt', 'ironman', 'germany'),        # after 2008: germany
    "maastricht_140.6":  Race('emea', 'maastricht', 'ironman', 'Maastricht'), 
    "austria_140.6":  Race('emea', 'austria', 'ironman', 'austria'), 
    "vichy_140.6":  Race('emea', 'vichy', 'ironman', 'vichy'), 
    "switzerland_140.6":  Race('emea', 'switzerland', 'ironman', 'switzerland'), 
    "france_140.6":  Race('emea', 'france', 'ironman', 'france'), 
    "mallorca_140.6":  Race('emea', 'mallorca', 'ironman', 'mallorca'), 
    "barcelona_140.6":  Race('emea', 'barcelona', 'ironman', 'barcelona'), 
    "southafrica_140.6":  Race('emea', 'south-africa', 'ironman', 'southafrica'),     

    "westernaustralia_140.6":  Race('asiapac', 'western-australia', 'ironman', 'westernaustralia'), 
    "australia_140.6":  Race('asiapac', 'australia', 'ironman', 'australia'), 
    "cairns_140.6":  Race('asiapac', 'cairns', 'ironman', 'cairns'), 
    "japan_140.6":  Race('asiapac', 'japan', 'ironman', 'japan'), 
    "taiwan_140.6":  Race('asiapac', 'taiwan', 'ironman', 'taiwan'), 
    "malaysia_140.6":  Race('asiapac', 'malaysia', 'ironman', 'malaysia'), 
    "korea_140.6":  Race('asiapac', 'korea', 'ironman', 'korea'),    

    'laketahoe_140.6': Race('americas', 'lake-tahoe', 'ironman', 'laketahoe'),
    'newyork_140.6': Race('americas', 'new-york', 'ironman', 'uschampionship'),
    'stgeorge_140.6': Race('americas', 'st.-george', 'ironman', 'st.george'),
    'muskoka_140.6': Race('americas', 'muskoka', 'ironman', 'muskoka'),
    "vineman_140.6":  Race('americas', 'vineman', 'ironman', 'vineman'),
    "china_140.6":  Race('asiapac', 'china', 'ironman', 'china'),

    # 'racine_70.3': Race('americas','racine', 'ironman-70.3', 'racine70.3'),
    # 'austin_70.3': Race('americas','austin', 'ironman-70.3', 'longhorn70.3'),
    # 'miami_70.3': Race('americas','miami', 'ironman-70.3', 'miami70.3'),
    # 'loscabos_70.3': Race('americas','los-cabos', 'ironman-70.3', 'loscabos70.3'),
    # 'sanjuan_70.3': Race('americas','san-juan', 'ironman-70.3', 'sanjuan70.3'),
    # 'monterrey_70.3': Race('americas','monterrey', 'ironman-70.3', 'monterrey70.3'),


    # 'hefei_70.3': Race('asiapac','hefei', 'ironman-70.3', 'hefei70.3'),    

    'pula_70.3': Race('emea','pula', 'ironman-70.3', 'pula70.3'),
    'northcarolina_70.3': Race('americas','north-carolina', 'ironman-70.3', 'northcarolina70.3'),
    'turkey_70.3': Race('emea','turkey', 'ironman-70.3', 'turkey70.3'),
    'miami_70.3': Race('americas','miami', 'ironman-70.3', 'miami70.3'),
    'austin_70.3': Race('americas','austin', 'ironman-70.3', 'austin70.3'),
    'loscabos_70.3': Race('americas','los-cabos', 'ironman-70.3', 'loscabos70.3'),
    'riodejaneiro_70.3': Race('americas','rio-de-janeiro', 'ironman-70.3', 'riodejaneiro70.3'),
    'xiamen_70.3': Race('asiapac','xiamen', 'ironman-70.3', 'xiamen70.3'),
    'uruguay_70.3': Race('americas','uruguay', 'ironman-70.3', 'uruguay70.3'),
    'westernsydney_70.3': Race('asiapac','western-sydney', 'ironman-70.3', 'sydney70.3'),
    'thailand_70.3': Race('asiapac','thailand', 'ironman-70.3', 'thailand70.3'),
    'westernaustralia_70.3': Race('asiapac','western-australia', 'ironman-70.3', 'westernaustralia70.3'),
    'cartagena_70.3': Race('americas','cartagena', 'ironman-70.3', 'cartagena70.3'),
    'taupo_70.3': Race('asiapac','taupo', 'ironman-70.3', 'taupo70.3'),  # pdf result: need to handle later
    'bahrain_70.3': Race('emea','bahrain', 'ironman-70.3', 'bahrain70.3'),
    'ballarat_70.3': Race('asiapac','ballarat', 'ironman-70.3', 'ballarat70.3'),
    'pucon_70.3': Race('americas','pucon', 'ironman-70.3', 'pucon70.3'),
    'dubai_70.3': Race('emea','dubai', 'ironman-70.3', 'dubai70.3'),
    'southafrica_70.3': Race('emea','south-africa', 'ironman-70.3', 'southafrica70.3'),
    'geelong_70.3': Race('asiapac','geelong', 'ironman-70.3', 'geelong70.3'),
    'subicbayphilippines_70.3': Race('asiapac','subic-bay-philippines', 'ironman-70.3', 'subicbay70.3'),  # need new record parser
    'buenosaires_70.3': Race('americas','buenos-aires', 'ironman-70.3', 'buenosaires70.3'),
    'sanjuan_70.3': Race('americas','san-juan', 'ironman-70.3', 'sanjuan70.3'),
    'campeche_70.3': Race('americas','campeche', 'ironman-70.3', 'campeche70.3'),
    'taiwan_70.3': Race('asiapac','taiwan', 'ironman-70.3', 'taiwan70.3'),
    'liuzhou_70.3': Race('asiapac','liuzhou', 'ironman-70.3', 'liuzhou70.3'),
    'oceanside_70.3': Race('americas','oceanside', 'ironman-70.3', 'california70.3'),
    'putrajaya_70.3': Race('asiapac','putrajaya', 'ironman-70.3', 'putrajaya70.3'),
    'texas_70.3': Race('americas','texas', 'ironman-70.3', 'lonestar70.3'),
    'florida_70.3': Race('americas','florida', 'ironman-70.3', 'florida70.3'),
    'palmas_70.3': Race('americas','palmas', 'ironman-70.3', 'brazil70.3'),
    'neworleans_70.3': Race('americas','new-orleans', 'ironman-70.3', 'neworleans70.3'),
    'peru_70.3': Race('americas','peru', 'ironman-70.3', 'peru70.3'),
    'stgeorge_70.3': Race('americas','st.-george', 'ironman-70.3', 'stgeorge70.3'),
    'portmacquarie_70.3': Race('asiapac','port-macquarie', 'ironman-70.3', 'portmacquarie70.3'),
    'vietnam_70.3': Race('asiapac','vietnam', 'ironman-70.3', 'Vietnam70.3'),
    'busselton_70.3': Race('asiapac','busselton', 'ironman-70.3', 'busselton70.3'),
    'stcroix_70.3': Race('americas','st.-croix', 'ironman-70.3', 'stcroix70.3'),
    'santarosa_70.3': Race('americas','santa-rosa', 'ironman-70.3', 'santarosa70.3'),
    'mallorca_70.3': Race('emea','mallorca', 'ironman-70.3', 'mallorca70.3'),    
    'paysdaix_70.3': Race('emea','pays-d-aix', 'ironman-70.3', 'france70.3'),
    'monterrey_70.3': Race('americas','monterrey', 'ironman-70.3', 'monterrey70.3'),
    'stpoelten_70.3': Race('emea','st.-poelten', 'ironman-70.3', 'austria70.3'),
    'chattanooga_70.3': Race('americas','chattanooga', 'ironman-70.3', 'chattanooga70.3'),
    'barcelona_70.3': Race('emea','barcelona', 'ironman-70.3', 'barcelona70.3'),
    'qujing_70.3': Race('asiapac','qujing', 'ironman-70.3', 'qujing70.3'),
    'hawaii_70.3': Race('americas','hawaii', 'ironman-70.3', 'honu70.3'),
    'raleigh_70.3': Race('americas','raleigh', 'ironman-70.3', 'raleigh70.3'),
    'victoria_70.3': Race('americas','victoria', 'ironman-70.3', 'victoria70.3'),
    'switzerland_70.3': Race('emea','switzerland', 'ironman-70.3', 'switzerland70.3'),
    'wisconsin_70.3': Race('americas','wisconsin', 'ironman-70.3', 'wisconsin70.3'),
    'cairns_70.3': Race('asiapac','cairns', 'ironman-70.3', 'cairns70.3'),
    'kraichgau_70.3': Race('emea','kraichgau', 'ironman-70.3', 'kraichgau70.3'),
    'eagleman_70.3': Race('americas','eagleman', 'ironman-70.3', 'eagleman70.3'),
    'japan_70.3': Race('asiapac','japan', 'ironman-70.3', 'japan70.3'),
    'durban_70.3': Race('emea','durban', 'ironman-70.3', 'durban70.3'),
    'syracuse_70.3': Race('americas','syracuse', 'ironman-70.3', 'syracuse70.3'),
    'italy_70.3': Race('emea','italy', 'ironman-70.3', 'italy70.3'),
    'luxembourg_70.3': Race('emea','luxembourg', 'ironman-70.3', 'luxembourg70.3'),
    'costarica_70.3': Race('americas','costa-rica', 'ironman-70.3', 'costarica70.3'),
    'elsinore_70.3': Race('emea','elsinore', 'ironman-70.3', 'kronborg70.3'),
    'staffordshire_70.3': Race('emea','staffordshire', 'ironman-70.3', 'staffordshire70.3'),
    'busankorea_70.3': Race('asiapac','busan-korea', 'ironman-70.3', 'busan70.3'),
    'monttremblant_70.3': Race('americas','mont-tremblant', 'ironman-70.3', 'monttremblant70.3'),
    'coeurdalene_70.3': Race('americas','coeur-d-alene', 'ironman-70.3', 'coeurdalene70.3'),
    'uk_70.3': Race('emea','uk', 'ironman-70.3', 'uk70.3'),
    'buffalospringslake_70.3': Race('americas','buffalo-springs-lake', 'ironman-70.3', 'buffalosprings70.3'),
    'haugesund_70.3': Race('emea','haugesund', 'ironman-70.3', 'Haugesund70.3'),
    'muncie_70.3': Race('americas','muncie', 'ironman-70.3', 'muncie70.3'),
    'hefei_70.3': Race('asiapac','hefei', 'ironman-70.3', 'hefei70.3'),
    'muskoka_70.3': Race('americas','muskoka', 'ironman-70.3', 'muskoka70.3'),
    'racine_70.3': Race('americas','racine', 'ironman-70.3', 'racine70.3'),
    'calgary_70.3': Race('americas','calgary', 'ironman-70.3', 'calgary70.3'),
    'canada_70.3': Race('americas','canada', 'ironman-70.3', 'canada70.3'),
    'ecuador_70.3': Race('americas','ecuador', 'ironman-70.3', 'ecuador70.3'),
    'otepaa_70.3': Race('emea','otepaa', 'ironman-70.3', 'otepaa70.3'),
    'boulder_70.3': Race('americas','boulder', 'ironman-70.3', 'boulder70.3'),
    'gdynia_70.3': Race('emea','gdynia', 'ironman-70.3', 'gdynia70.3'),
    'philippines_70.3': Race('asiapac','philippines', 'ironman-70.3', 'philippines70.3'),
    'steelhead_70.3': Race('americas','steelhead', 'ironman-70.3', 'steelhead70.3'),
    'chungjukorea_70.3': Race('asiapac','chungju-korea', 'ironman-70.3', 'chungju70.3'),
    'bintan_70.3': Race('asiapac','bintan', 'ironman-70.3', 'bintan70.3'),
    'dublin_70.3': Race('emea','dublin', 'ironman-70.3', 'dublin70.3'),
    'timberman_70.3': Race('americas','timberman', 'ironman-70.3', 'timberman70.3'),
    'vichy_70.3': Race('emea','vichy', 'ironman-70.3', 'vichy70.3'),
    'fozdoiguacu_70.3': Race('americas','foz-do-iguacu', 'ironman-70.3', 'fozdoiguacu70.3'),
    'zellamseekaprun_70.3': Race('emea','zell-am-see-kaprun', 'ironman-70.3', 'salzburg70.3'),
    'cascais_70.3': Race('emea','cascais', 'ironman-70.3', 'cascais70.3'),
    'worldchampionship_70.3': Race('americas','world-championship', 'ironman-70.3', 'worldchampionship70.3'),
    'ruegen_70.3': Race('emea','ruegen', 'ironman-70.3', 'ruegen70.3'),
    'santacruz_70.3': Race('americas','santa-cruz', 'ironman-70.3', 'santacruz70.3'),
    'lakeplacid_70.3': Race('americas','lake-placid', 'ironman-70.3', 'lakeplacid70.3'),
    'atlanticcity_70.3': Race('americas','atlantic-city', 'ironman-70.3', 'atlanticcity70.3'),
    'weymouth_70.3': Race('emea','weymouth', 'ironman-70.3', 'weymouth70.3'),
    'chongqing_70.3': Race('asiapac','chongqing', 'ironman-70.3', 'chongqing70.3'),
    'lanzarote_70.3': Race('emea','lanzarote', 'ironman-70.3', 'lanzarote70.3'),
    'guryekorea_70.3': Race('asiapac','gurye-korea', 'ironman-70.3', 'korea70.3'),
    'augusta_70.3': Race('americas','augusta', 'ironman-70.3', 'augusta70.3'),
    'superfrog_70.3': Race('americas','superfrog', 'ironman-70.3', 'superfrog70.3'),

    'ohio_70.3': Race('americas','ohio', 'ironman-70.3', 'ohio70.3'),
    'cozumel_70.3': Race('americas','cozumel', 'ironman-70.3', 'cancun70.3'),
    
    'arizona_70.3': Race('americas','arizona', 'ironman-70.3', 'Arizona70.3'),
    'jonkoping_70.3': Race('emea','jonkoping', 'ironman-70.3', 'joenkoeping70.3'),
}

pool = ThreadPool(processes=20)

root = ''

if __name__ == "__main__":    

    race = race_dict[sys.argv[1]]
    start_year = int(sys.argv[2])
    end_year = int(sys.argv[3])
    years = list(range(start_year, end_year + 1))
    as_of_year = sys.argv[4] 
    root = sys.argv[5]
    print(race)    
    print(start_year)
    print(end_year)
    print(root)
    print(years)
    race.get(years, as_of_year)
    pass
    # root=''
    # if len(sys.argv) > 2:
    #     root = sys.argv[2]    
    
    # for r in race_list:       
    #     result = r.get([int(sys.argv[1])])
    #     file_name = os.path.join(root, result['name'])
    #     print(file_name)
    #     with open(root + result["name"], 'w', newline='') as csvfile:
    #         writer = CsvWriter(csvfile)
    #         writer.writerow(result["header"])        
    #         writer.writerows(result["rows"]);        