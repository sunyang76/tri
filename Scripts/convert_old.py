import csv
import sys
import re

class CsvWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f):        
        self.writer = csv.writer(f, delimiter='|')

    def writerow(self, row):
        self.writer.writerow(row)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

def convert(in_file, out_file):
    newdata = [];
    with open(in_file, "r", encoding='utf-8') as theFile:        
        reader = csv.DictReader( theFile, delimiter='|' )
        for line in reader:
            newline = {}
            newline['region'] = 'asiapac'
            newline['race'] = line['race']
            newline['type'] = 'ironman'
            newline['date'] = line['date']
            newline['as_of_year'] = '2015'
            newline['transition_t2'] = line['transition_t2'] == '---' if '' else line['transition_t2']
            newline['transition_t1'] = line['transition_t1'] == '---' if '' else line['transition_t1']

            if newline['transition_t2'] == '---':
                newline['transition_t2'] = ''


            if newline['transition_t1'] == '---':
                newline['transition_t1'] = ''

            

            if re.match("\d+", line['header_overall_rank']):
                newline['header_status'] = 'FINISH'
            else:
                newline['header_status'] = line['header_overall_rank']                
            
            newline['header_div_rank'] = line['header_div_rank'] if line['header_div_rank'] != '---' else  line['header_div_rank'] 
            newline['header_gender_rank'] = line['header_gender_rank'] if line['header_gender_rank'] != '---' else  line['header_gender_rank'] 

            if re.match("\d+", line['header_overall_rank']):
                newline['header_overall_rank'] = line['header_overall_rank']
            else:
                newline['header_overall_rank'] = ''

            newline['header_overall_rank'] = line['header_overall_rank'] if line['header_gender_rank'] != '---' else  line['header_gender_rank'] 

            newline['bike_pace'] = line['bike_pace'] if line['bike_pace'] != '---' else  line['bike_pace'] 
            newline['bike_race_time'] = line['bike_race_time'] if line['bike_race_time'] != '---' else  line['bike_race_time'] 
            newline['bike_gender_rank'] = line['bike_gender_rank'] if line['bike_gender_rank'] != '---' else  line['bike_gender_rank'] 
            newline['bike_distance'] = line['bike_distance'] if line['bike_distance'] != '---' else  line['bike_distance'] 
            newline['bike_split_time'] = line['bike_split_time'] if line['bike_split_time'] != '---' else  line['bike_split_time'] 
            newline['bike_overall_rank'] = line['bike_overall_rank'] if line['bike_overall_rank'] != '---' else  line['bike_overall_rank'] 
            newline['bike_division_rank'] = line['bike_division_rank'] if line['bike_division_rank'] != '---' else  line['bike_division_rank'] 
            newline['general_bib'] = line['general_bib'] if line['general_bib'] != '---' else  line['general_bib'] 
            newline['general_points'] = line['general_points'] if line['general_points'] != '---' else  line['general_points'] 
            newline['general_state'] = line['general_state']  if line['general_state'] != '---' else  line['general_state'] 
            newline['general_profession'] = '' 
            newline['general_country'] = line['general_country'] if line['general_country'] != '---' else  line['general_country'] 
            newline['general_age'] = line['general_age'] if line['general_age'] != '---' else  line['general_age'] 

            newline['general_division'] = line['general_division'] if line['general_division'] != '---' else  line['general_division'] 
            newline['run_pace'] = line['run_pace'] if line['run_pace'] != '---' else  line['run_pace'] 
            newline['run_race_time'] = line['run_race_time'] if line['run_race_time'] != '---' else  line['run_race_time'] 
            newline['run_gender_rank'] = line['run_gender_rank'] if line['run_gender_rank'] != '---' else  line['run_gender_rank'] 
            newline['run_distance'] = line['run_distance'] if line['run_distance'] != '---' else  line['run_distance'] 
            newline['run_split_time'] = line['run_split_time'] if line['run_split_time'] != '---' else  line['run_split_time'] 
            newline['run_overall_rank'] = line['run_overall_rank'] if line['run_overall_rank'] != '---' else  line['run_overall_rank'] 
            newline['run_division_rank'] = line['run_division_rank'] if line['run_division_rank'] != '---' else  line['run_division_rank'] 
            newline['summary_bike'] = line['summary_bike'] if line['summary_bike'] != '---' else  line['summary_bike'] 
            newline['summary_swim'] = line['summary_swim'] if line['summary_swim'] != '---' else  line['summary_swim'] 
            newline['summary_overall'] = line['summary_overall'] if line['summary_overall'] != '---' else  line['summary_overall'] 
            newline['summary_run'] = line['summary_run'] if line['summary_run'] != '---' else  line['summary_run'] 

            newline['swim_pace'] = line['swim_pace'] if line['swim_pace'] != '---' else  line['swim_pace'] 
            newline['swim_race_time'] = line['swim_race_time']  if line['swim_race_time'] != '---' else  line['swim_race_time'] 
            newline['swim_gender_rank'] = line['swim_gender_rank'] if line['swim_gender_rank'] != '---' else  line['swim_gender_rank'] 
            newline['swim_distance'] = line['swim_distance'] if line['swim_distance'] != '---' else  line['swim_distance'] 
            newline['swim_split_time'] = line['swim_split_time']  if line['swim_split_time'] != '---' else  line['swim_split_time'] 
            newline['swim_overall_rank'] = line['swim_overall_rank']  if line['swim_overall_rank'] != '---' else  line['swim_overall_rank'] 
            newline['swim_division_rank'] = line['swim_division_rank']  if line['swim_division_rank'] != '---' else  line['swim_division_rank'] 

            idx_space =  line['header_name'].index(' ')

            newline['name_last'] = line['header_name'][:idx_space].strip()
            newline['name_first'] = line['header_name'][idx_space+ 1:].strip()
            newline['name_gender'] = line['gender'] if line['gender'] != '---' else  line['gender'] 

            for k in newline.keys():
                if newline[k] == '---':
                    newline[k] = ''
                if k != 'header_status' and newline[k] in ['DNF', 'DQ', 'DNS']:
                    newline[k] = ''
            # for k in newline.keys():
            #     print(k + " = " + newline[k])
            newdata.append(newline)            
           

    with open(out_file, 'w', newline='', encoding='utf-8') as csvfile:
        w = csv.DictWriter(csvfile, newdata[0].keys(), delimiter='|')
        w.writeheader()
        for r in newdata:
            w.writerow(r)
     
        # reader = csv.DictReader( theFile, delimiter='|' )
        # for line in reader:
        #     sql = to_sql(line)          
        #     # print(sql)
        #     try:
        #         cur.execute(sql)
        #     except:
        #         print(sql)
        #     # break         

if __name__ == "__main__":
    if len(sys.argv) > 1:
        in_file = sys.argv[1]
        out_file = sys.argv[2]
        convert(in_file, out_file)
        
