use wtc;

db.getCollection('race_result').createIndex(
    {"year":1, "race.type": 1, "race.region": 1, "race.code": 1},
    {"name": "race_index"}
)

db.getCollection('race_result').createIndex(
    {"general.first":1},
    {"name": "first_index"}
)
    
db.getCollection('race_result').createIndex(
    {"general.last":1},
    {"name": "last_index"}
)
