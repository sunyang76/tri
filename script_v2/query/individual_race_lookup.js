use wtc;

var race_lookup = db.result_raw.aggregate([ 
    {"$group": {
        "_id": {
            "year": '$year',
            'type': '$race.type',            
            'region': '$race.region',
            'code': '$race.code',
            'name': '$race.name'
        }
    }},
    {
        "$project": {
            '_id': 0,
            'year': '$_id.year',
            'type': '$_id.type',
            'region': '$_id.region',
            'code': '$_id.code',
            'name': '$_id.name'
        }
    }
])
    
race_lookup.forEach(function(item){
    db.race_lookup.save(item)
})