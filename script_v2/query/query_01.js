// db.result.find({'general.gender': 'M', 'general.first': 'Phillip', 'general.last': 'Anderson'}).limit(100)


db.result.aggregate([
    {$match: {$and: [{'general.gender': 'M', 'general.country': 'USA', 'status': 'FINISH', 'race.type': 'ironman'}, {$or: [
       {'general.first': /^BING$/i, 'general.last': /^LIU$/i,},
//         {'general.first': /^PHILLIP$/i, 'general.last': /^ANDERSON$/i,},
//         {'general.first': /^SAM$/i, 'general.last': /^AZLEIN$/i,},
//         {'general.first': /^WALTER$/i, 'general.last': /^COSBY$/i,},
//         {'general.first': /^BILL$/i, 'general.last': /^DAVIS$/i,},
//         {'general.first': /^MARK$/i, 'general.last': /^DELORME$/i,},
//         {'general.first': /^BARRY$/i, 'general.last': /^FAUST$/i,},
//         {'general.first': /^JURGEN$/i, 'general.last': /^FISCHER$/i,},
//         {'general.first': /^GUSTAVO$/i, 'general.last': /^GIUDICE$/i,},
//         {'general.first': /^MARK$/i, 'general.last': /^GRESZLER$/i,},        
//         {'general.first': /^GARY$/i, 'general.last': /^GRUENISEN$/i,},
//         {'general.first': /^TOM$/i, 'general.last': /^HENNESSY$/i,},
//         {'general.first': /^MIKE$/i, 'general.last': /^JOVANOVICH$/i,},
//         {'general.first': /^WILLIAM$/i, 'general.last': /^KAEWERT$/i,},      
//         {'general.first': /^KEITH$/i, 'general.last': /^LAIRD$/i,},
//         {'general.first': /^DAVID$/i, 'general.last': /^LANDMAN$/i,},    
//         {'general.first': /^BING$/i, 'general.last': /^LIU$/i,},
//         {'general.first': /^KURT$/i, 'general.last': /^MADDEN$/i,},
//         {'general.first': /^JAMES$/i, 'general.last': /^MCCARTHY$/i,},
//         {'general.first': /^KELLY$/i, 'general.last': /^MONAHAN$/i,},        
//         {'general.first': /^CARL$/i, 'general.last': /^MORRISHOW$/i,},
//         {'general.first': /^JOHN$/i, 'general.last': /^MOZENA$/i,},
//         {'general.first': /^WILL$/i, 'general.last': /^MURRAY$/i,},       
//         {'general.first': /^JONATHAN$/i, 'general.last': /^NISSANOV$/i,},
//         {'general.first': /^MICHAEL$/i, 'general.last': /^PARKER$/i,},
//         {'general.first': /^JUAN CARLOS$/i, 'general.last': /^PARODI$/i,},        
//         {'general.first': /^GUY$/i, 'general.last': /^SIGLEY$/i,},
//         {'general.first': /^ROBERT$/i, 'general.last': /^SMITH$/i,},
//         {'general.first': /^QUAY$/i, 'general.last': /^SNYDER$/i,},
//         {'general.first': /^T RUSSELL$/i, 'general.last': /^STROBRIDGE$/i,},
//         {'general.first': /^JEFF$/i, 'general.last': /^VIOLA$/i,},
//         {'general.first': /^WAYNE$/i, 'general.last': /^WALLACE$/i,},
//         {'general.first': /^BRADLEY$/i, 'general.last': /^WOODIEL$/i,},
//         {'general.first': /^GREGG$/i, 'general.last': /^ZAFFARONI$/i,}        
    ]}]}},
    {$limit:500 },
    {$sort: {'year':1, 'race.code': 1}},
    {$project: {
        '_id': 0,         
        'year': '$year',
        'race': '$race.code',
        'status': '$status',
        'type': '$race.type',
        'first': '$general.first',
        'last': '$general.last',
        'country': '$general.country',
        'profession': '$general.profession',
        'division': '$general.division',
        'overall': '$summary.overall',
        'swim': '$summary.swim',
        'bike': '$summary.bike',
        'run': '$summary.run',
//         'overall_rank': '$summary.overall_rank',
//         'gender_rank': '$summary.gender_rank',
//         'div_rank': '$summary.div_rank'
        }
    }   
])
    /*
    ['_batch'];

var filtered_result = [];
for(var i = 0; i < ret.length; i++) {
    var rec = ret[i];

    var div_parts = rec.division.split('-');
    var low = (div_parts[0] * 1+ (2017-year))
    var high = (div_parts[1] * 1+ (2017-year))
        // 60 ~ 64
    if(low < 64 && 60 <= high) {
       filtered_result.push(rec);
    }

}    
  
print(filtered_result);
*/
    