use wtc;

db.getCollection('name_lookup').aggregate([
    { '$match': { 'country': 'CHN' } },
    { "$group": { _id: "$last", count: { $sum: 1 } } }, {
        $project: {
            _id: 0,
            last: '$_id',
            count: 1
        }
    },
    { $sort: { count: -1 } }
])
