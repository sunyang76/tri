use wtc;

var races = db.race_lookup.aggregate([ 
    {"$group": {
        "_id": {            
            'type': '$type',            
            'region': '$region',
            'code': '$code',
            'name': '$name'
        }
    }},
    {
        "$project": {
            '_id': 0,            
            'type': '$_id.type',
            'region': '$_id.region',
            'code': '$_id.code',
            'name': '$_id.name'
        }
    }
])

races.forEach(function(item){
    db.race.save(item)
})
