use wtc

var name_lookup = db.race_result.aggregate([ 
    {'$group': {
        '_id': {
            'first': '$general.first',
            'last': '$general.last',
            'full': '$general.full'
        }
    }},
    {
        '$project': {
            '_id': 0,
            'first': '$_id.first',
            'last': '$_id.last',
            'full': '$_id.full'
        }
    }
])
    
name_lookup.forEach(function(item){
    db.name_lookup.save(item)
})
