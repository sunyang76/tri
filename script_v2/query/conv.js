use wtc;

function toDistance(str_distance) {
    if (str_distance) {
        var re = /(\d*\.?\d*)\s*([a-z]*)/g;
        var matched = re.exec(str_distance.toLowerCase());

        if (matched[1] === '') {
            return null;
        } else {
            if (matched[2] === 'km') {
                return (matched[1] * 0.621371).toFixed(1) * 1;
            } else {
                return matched[1] * 1;
            }

        }
    }
    return null;
}

function toSec(time_str) {
    if (time_str) {
        var parts = time_str.split(':');
        var sec = null
        switch (parts.length) {
            case 3:
                sec = parts[0] * 3600 + parts[1] * 60 + parts[2] * 1;
                break;
            case 2:
                sec = parts[0] * 60 + parts[1] * 1
                break;
            case 1:
                sec = parts[0] * 1;
                break;
        }
        return sec
    } else {
        return null;
    }
}

function toSwimPace(str_pace) {
    if (str_pace) {
        return toSec(str_pace.split('/')[0]);
    } else {
        return null;
    }
}


function toNumber(num_str) {
    if (num_str) {
        return num_str * 1;
    } else {
        return null;
    }
}

function capitalizeFirstLetter(string) {

    if (string) {

        string = string.trim();

        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();

    } else {

        return null;

    }

}


function convert_general(general) {

    if (general) {

        general.first = capitalizeFirstLetter(general.first);

        general.last = capitalizeFirstLetter(general.last);

        general.full = (general.first || '') + ' ' + (general.last || '').trim();

    }

}


function convert_summary(summary) {
    if (summary) {
        summary.div_rank = toNumber(summary.div_rank);
        summary.gender_rank = toNumber(summary.gender_rank);
        summary.overall_rank = toNumber(summary.overall_rank);
        summary.swim = toSec(summary.swim);
        summary.bike = toSec(summary.bike);
        summary.run = toSec(summary.run);
        summary.overall = toSec(summary.overall);
    }
}

function convert_transition(transition) {
    if (transition) {
        transition.t1 = toSec(transition.t1);
        transition.t2 = toSec(transition.t2);
    }
}

function convert_swim(swim) {
    if (swim) {
        swim.forEach(function(split) {
            split.div_rank = toNumber(split.div_rank);
            split.gender_rank = toNumber(split.gender_rank);
            split.overall_rank = toNumber(split.overall_rank);
            split.race_time = toSec(split.race_time);
            split.split_time = toSec(split.split_time);
            split.distance = toDistance(split.distance);
            split.pace = toSwimPace(split.pace);
        });
    }
}

function convert_bike(bike) {
    if (bike) {
        bike.forEach(function(split) {
            split.div_rank = toNumber(split.div_rank);
            split.gender_rank = toNumber(split.gender_rank);
            split.overall_rank = toNumber(split.overall_rank);
            split.race_time = toSec(split.race_time);
            split.split_time = toSec(split.split_time);
            split.distance = toDistance(split.distance);
            split.pace = split.distance && split.split_time ? (split.distance / split.split_time * 3600).toFixed(2) * 1 : null;
        });
    }
}

function convert_run(run) {
    if (run) {
        run.forEach(function(split) {
            split.div_rank = toNumber(split.div_rank);
            split.gender_rank = toNumber(split.gender_rank);
            split.overall_rank = toNumber(split.overall_rank);
            split.race_time = toSec(split.race_time);
            split.split_time = toSec(split.split_time);
            split.distance = toDistance(split.distance);
            split.pace = split.distance && split.split_time ? (split.split_time / split.distance).toFixed(2) * 1 : null;
        });
    }
}


var result_raw = db.result_raw;
// var results = result_raw.find({'year':2016, 'race.type':'ironman', 'race.region':'americas', 'race.code': 'wisconsin', 'status':'FINISH'});
var results = result_raw.find({});
var race_result = db.race_result;


var index = 1;
results.forEach(function(item) {
    print('convert ' + index);
    try {

        convert_general(item.general);
        convert_summary(item.summary);
        convert_transition(item.transition);
        convert_swim(item.swim)
        convert_bike(item.bike);
        convert_run(item.run);
        race_result.save(item);
    } catch (e) {
        print(item);
        throw e;
    }
    index = index + 1;
});
