import urllib
import codecs
import sys
from urllib.request import urlopen
from collections import namedtuple
from operator import attrgetter, itemgetter
from multiprocessing.pool import ThreadPool
from bs4 import BeautifulSoup
from datetime import datetime
import re
import os
import json
import logging
import html

Race = namedtuple('Race', ['region', 'name', 'type', 'code'])
RaceRecord = namedtuple('RaceRecord',
                        ['region', 'name', 'year', 'status', 'div', 'bib', 'country', 'gender', 'first_name',
                         'last_name', 'full_name', 'total_time', 'swim_time', 'bike_time', 'run_time', 'div_rank',
                         'gender_rank', 'overall_rank'])
Detail = namedtuple('Detail', ['year', 'race', 'status', 'general', 'summary', 'swim', 'bike', 'run', 'transition'])
DetailGeneral = namedtuple('DetailGeneral',
                           ['bib', 'first', 'last', 'division', 'gender', 'city', 'state', 'country', 'profession'])
DetailSummary = namedtuple('DetailSummary',
                           ['swim', 'bike', 'run', 'overall', 'div_rank', 'gender_rank', 'overall_rank'])
DetailSplit = namedtuple('DetailSplit',
                         ['name', 'distance', 'split_time', 'pace', 'race_time', 'div_rank', 'gender_rank',
                          'overall_rank'])
DetailTransition = namedtuple('DetailTransition', ['t1', 't2'])

gender_lookup = {
    'male': 'M',
    'female': 'F'
}

race_code_lookup = {
    # 140.6
    'world-championship': 'worldchampionship',
    'mont-tremblant': 'monttremblant',
    'lake-placid': 'lakeplacid',
    'coeur-dalene': 'coeurdalene',
    'los-cabos': 'loscabos',
    'frankfurt': 'germany',
    'south-africa': 'southafrica',
    'western-australia': 'westernaustralia',
    'lake-tahoe': 'laketahoe',
    'new-york': 'uschampionship',
    'st.-george': 'st.george',

    # 70.3
    'north-carolina': 'northcarolina',
    #'los-cabos': 'loscabos',
    'rio-de-janeiro': 'riodejaneiro',
    'austin': 'longhorn70.3'    
}

disc_race = [
    Race('americas', 'lake-tahoe', 'ironman', 'laketahoe'),
    Race('americas', 'new-york', 'ironman', 'uschampionship'),
    Race('americas', 'st.-george', 'ironman', 'st.george'),
    Race('americas', 'muskoka', 'ironman', 'muskoka'),
    Race('americas', 'vineman', 'ironman', 'vineman'),
    Race('asiapac', 'china', 'ironman', 'china'),
    Race('asiapac', 'japan', 'ironman', 'japan'), 
    Race('asiapac', 'korea', 'ironman', 'korea'),
    Race('asiapac', 'china', 'ironman-70.3', 'china')
]

pool = ThreadPool(processes=20)


def get_ironman_race():
    race_list = []
    response = urlopen('http://www.ironman.com/events/triathlon-races.aspx?d=ironman')
    html = response.read().decode('utf-8', 'ignore')
    race_link = re.findall("\"http://www.ironman.com/triathlon/events/(\w+)/ironman/(.+).aspx\" class=\"eventDetails\"",
                           html)
    for r in race_link:
        region = r[0]
        name = r[1]
        race_type = 'ironman'

        code = name

        if name in race_code_lookup:
            code = race_code_lookup[name]

        race = Race(region=region, name=name, type=race_type, code=code)

        race_list.append(race)

    return sorted(race_list, key=attrgetter('type', 'region'))


def get_ironman703_race():
    race_list = []
    response = urlopen('http://www.ironman.com/events/triathlon-races.aspx?d=ironman+70.3')
    html = response.read().decode('utf-8', 'ignore')
    race_link = re.findall(
        "\"http://www.ironman.com/triathlon/events/(\w+)/ironman-70.3/(.+).aspx\" class=\"eventDetails\"", html)
    for r in race_link:
        region = r[0]
        name = r[1]
        race_type = 'ironman-70.3'

        code = name
        if name in race_code_lookup:
            code = race_code_lookup[name]        

        race = Race(region=region, name=name, type=race_type, code=name + '70.3')
        race_list.append(race)

    return sorted(race_list, key=attrgetter('type', 'region'))


def get_race_folder(root, r):
    folder = os.path.join(root, r.type, r.region, r.name)
    return folder


def create_data_folder(root, race_list):
    for r in race_list:
        folder = get_race_folder(root, r)
        if not os.path.exists(folder):
            os.makedirs(folder)


def get_years(race):
    print('get race collection for {0}'.format(race))
    url = 'http://m.ironman.com/triathlon/events/{0}/{1}/{2}/results.aspx'.format(race.region, race.type, race.name)
    try:
        response = urlopen(url)
        html = response.read().decode('utf-8', 'ignore')
        years = re.findall("<option value=.+>(\d\d\d\d)</option>", html)

        return sorted([int(y) for y in years], reverse=True)
    except:
        print('can not find race {0}'.format(race))
        return []

def get_status(total_time):    
    pattern = re.compile('\d+:\d+:\d+')
    ret = total_time
    if pattern.match(total_time):        
        ret = 'FINISH'
    
    return ret


def get_total_time(total_time):
    pattern = re.compile('\d+:\d+:\d+')
    ret = total_time
    if not pattern.match(total_time):
        ret = None
    return ret;


def get_race_year(race, year, check_exist=None):
    ret = []
    if check_exist != None:
        race_file = get_race_file_path(check_exist['root'], race, year)
        print(race_file)
        if os.path.exists(race_file):
            print('race {0} on {1} already been processed. skip'.format(race.name, year))
            return ret;

    url = 'http://m.ironman.com/triathlon/events/{0}/{1}/{2}/results.aspx?year={3}'.format(race.region, race.type,
                                                                                           race.name, year)
    print(url)
    print('get year {0} for {1}'.format(year, race))
    response = None
    try:        
        response = urlopen(url)
    except urllib.error.HTTPError as error:
        if error.code == 404:
            return ret
        else:
            raise
    html = response.read().decode('utf-8', 'ignore')
    soup = BeautifulSoup(html, 'html.parser')

    # full_race_name = soup.find_all('div', {'text primary-text'})[0].text.strip()
    # print(full_race_name)
    # location_and_time = soup.find_all('div', {'text location-date'})[0]
    # race_location = location_and_time.contents[0].strip()
    # print(race_location)
    # race_time = datetime.strptime(location_and_time.contents[2].strip(), '%B %d, %Y').strftime('%Y-%m-%d')
    # print(race_time)

    records = soup.find_all('table')[0].find_all('tr')

    page_records = []

    for rec in records[1:]:
        div = rec.attrs['data-age']
        bib = rec.attrs['data-bib-number']
        country = rec.attrs['data-country'].upper()
        gender = gender_lookup[rec.attrs['data-gender']]

        result_page = rec.attrs['data-result-page']

        tds = rec.find_all('td')

        full_name = tds[1].contents[0].strip()
        name_parts = full_name.split(',')
        last_name = name_parts[0].strip()
        first_name = None

        if len(name_parts) > 1:
            first_name = name_parts[1].strip()

        total_time = tds[2].text
        swim_time = tds[3].text
        bike_time = tds[4].text
        run_time = tds[5].text
        div_rank = tds[6].text
        gender_rank = tds[7].text
        overall_rank = tds[8].text

        lite_rec = RaceRecord(region=race.region, name=race.name, year=year, status=get_status(total_time), div=div,
                              bib=bib,
                              country=country, gender=gender, first_name=first_name, last_name=last_name,
                              full_name=full_name, total_time=get_total_time(total_time), swim_time=swim_time,
                              bike_time=bike_time, run_time=run_time, div_rank=div_rank, gender_rank=gender_rank,
                              overall_rank=overall_rank)

        page_records.append((year, race, lite_rec))

    # break;

    ret = pool.map(get_detail_record, page_records)

    return ret


def get_detail_general(data, lite_rec, full_name):
    properties = data.find_all('tr')

    bib = None
    division = None
    gender = None
    state = None
    city = None
    country = None
    profession = None

    division = lite_rec.div
    gender = lite_rec.gender

    first = lite_rec.first_name
    last = lite_rec.last_name
    
    if lite_rec.full_name == None and full_name != None:
        index = full_name.rfind(' ')
        first = full_name[:index].strip()
        last = full_name[index:].strip()

    for p in properties:
        tds = p.find_all('td')
        if tds[0].text == 'BIB':
            bib = tds[1].text.strip()

        if tds[0].text == 'Division' and lite_rec.full_name == None:            
            div_text = tds[1].text.strip()            
            division = div_text[1:]
            gender = div_text[:1]
            if gender not in ['M', 'F']:
                gender = None

        if tds[0].text == 'State':  # TODO: split this into city and state. it not working well for china
            state = tds[1].text.strip()
        if tds[0].text == 'Country':
            country = tds[1].text.strip()
        if tds[0].text == 'Profession':
            profession = tds[1].text.strip()

    index = state.rfind(' ')
    if index > 0:
        city = state[:index].strip()
        state = state[index:].strip()

    return DetailGeneral(bib=bib, first=first, last=last, division=division, gender=gender,
                         city=city, state=state, country=country, profession=profession)


def get_detail_summary(data, lite_rec):
    properties = data.find_all('tr')

    swim = None
    bike = None
    run = None
    overall = None

    for p in properties:
        tds = p.find_all('td')
        if tds[0].text == 'Swim':
            swim = tds[1].text.strip()
        if tds[0].text == 'Bike':
            bike = tds[1].text.strip()
        if tds[0].text == 'Run':  # TODO: split this into city and state. it not working well for china
            run = tds[1].text.strip()
        if tds[0].text == 'Overall':
            overall = tds[1].text.strip()

    return DetailSummary(swim=swim, bike=bike, run=run, overall=overall, div_rank=lite_rec.div_rank,
                         gender_rank=lite_rec.gender_rank, overall_rank=lite_rec.overall_rank)


def get_detail_split(data):
    splits = []

    splits_items = data.find_all('tr')

    for item in splits_items[2:]:
        tds = item.find_all('td');

        name = tds[0].text.strip().replace(u'\xa0', ' ')
        distance = tds[1].text.strip()
        split_time = tds[2].text.strip()
        pace = tds[3].text.strip()
        race_time = tds[4].text.strip()
        div_rank = tds[5].text.strip()
        gender_rank = tds[6].text.strip()
        overall_rank = tds[7].text.strip()

        splits.append(DetailSplit(name, distance, split_time, pace, race_time, div_rank, gender_rank, overall_rank))

    return splits


def get_detail_transition(data):
    t1 = None
    t2 = None

    transitions = data.find_all('tr')

    t_len = len(transitions)

    for t in transitions:
        tds = t.find_all('td')
        if tds[0].text.strip()[:2] == 'T1':
            t1 = tds[1].text.strip()
        if tds[0].text.strip()[:2] == 'T2':
            t2 = tds[1].text.strip()

    return DetailTransition(t1, t2)


def get_detail_record(params):
    year = params[0]
    race = params[1]
    lite_rec = params[2]

    print('get detail for bib : {0}'.format(lite_rec.bib))

    try:
        url = 'http://tracking.ironmanlive.com/mobilesearch.php?y={0}&race={1}&v=3.0&athlete={2}'.format(year, race.code, lite_rec.bib)

        response = urlopen(url)
        html = response.read().decode('utf-8', 'ignore')
        soup = BeautifulSoup(html, 'html.parser')
        athlete_info = soup.find_all('div', attrs={'class': 'athlete-info'})[0]  # .find_all()
        full_name = athlete_info.find_all('h1')[0].text.strip()        
        ranks = athlete_info.find_all('p', attrs={'class': 'rank'})[0].find_all('span')
        div_rank = ranks[0].find_all('strong')[0].text.strip()
        gen_rank = ranks[1].find_all('strong')[0].text.strip()
        overall_rank = ranks[2].find_all('strong')[0].text.strip()

        tables = athlete_info.find_all('table')

        detail_general = get_detail_general(tables[0], lite_rec, full_name)
        detail_summary = get_detail_summary(tables[1], lite_rec)

        if len(tables) > 3:
            detail_swim = get_detail_split(tables[2])
            detail_bike = get_detail_split(tables[3])
            detail_run = get_detail_split(tables[4])

        if len(tables) > 3:
            detail_transition = get_detail_transition(tables[5])
        else:
            detail_transition = get_detail_transition(tables[2])

        status = lite_rec.status
    
        if lite_rec.status == None:            
            status = get_status(detail_summary.overall)

        return Detail(year=year, race=race, status=status, general=detail_general, summary=detail_summary,
                      swim=detail_swim, bike=detail_bike, run=detail_run, transition=detail_transition)
    except:    
        summary = DetailSummary(swim = lite_rec.swim_time, bike = lite_rec.bike_time, run=lite_rec.bike_time, overall = lite_rec.total_time, div_rank = lite_rec.div_rank, gender_rank = lite_rec.gender_rank, overall_rank = lite_rec.overall_rank)
        return Detail(year=year, race=race, status=lite_rec.status,
                      general=DetailGeneral(bib=lite_rec.bib, division=lite_rec.div, gender=lite_rec.gender,
                                            first=lite_rec.first_name, last=lite_rec.last_name, city=None, state=None,
                                            country=lite_rec.country, profession=None), summary=summary, swim=None,
                      bike=None, run=None, transition=None)


def get_race_file_path(root, race, year):
    race_folder = get_race_folder(root, race)
    race_file = '{0}_{1}_{2}.json'.format(race.name, race.type, year)
    full_race_file_path = os.path.join(race_folder, race_file)

    return full_race_file_path


def isnamedtupleinstance(x):  # thank you http://stackoverflow.com/a/2166841/6085135
    _type = type(x)
    bases = _type.__bases__
    if len(bases) != 1 or bases[0] != tuple:
        return False
    fields = getattr(_type, '_fields', None)
    if not isinstance(fields, tuple):
        return False
    return all(type(i) == str for i in fields)


def normalize_value(value):
    if isinstance(value, str):
        value = value.strip()
        if len(value) == 0:
            return None
        if value[:2] == '--' or value[:2] == '-' : 
            return None

        return value
    else:
        return value


def unpack(obj):
    if isinstance(obj, dict):
        return {key: unpack(value) for key, value in obj.items()}
    elif isinstance(obj, list):
        return [unpack(value) for value in obj]
    elif isnamedtupleinstance(obj):
        return {key: unpack(value) for key, value in obj._asdict().items()}
    elif isinstance(obj, tuple):
        return tuple(unpack(value) for value in obj)
    else:
        return normalize_value(obj)


def save_race(root, race, records, year, override=True):
    if len(records) == 0:
        print('no records to save')
        return

    record_array = [unpack(r) for r in records]

    race_folder = get_race_folder(root, race)

    if not os.path.exists(race_folder):
        os.makedirs(race_folder)

    full_race_file_path = get_race_file_path(root, race, year)

    if os.path.exists(full_race_file_path) and not override:
        print('file {0} already exist. skip save'.format(full_race_file_path))
    else:
        with open(full_race_file_path, 'w', encoding="utf-8") as outfile:
            print('save to {0}'.format(full_race_file_path))
            json.dump(record_array, outfile, sort_keys=True, indent=4, ensure_ascii=False)


def get_race_year_brutal(race, year, min_bib = 1, max_bib = 3000):    
    page_records = [];

    for i in range(min_bib, max_bib + 1):
        lite_rec = RaceRecord(region=race.region, name=race.name, year=year, status=None, div=None, bib=str(i),
                              country=None, gender=None, first_name=None, last_name=None, full_name=None,
                              total_time=None, swim_time=None,
                              bike_time=None, run_time=None, div_rank=None, gender_rank=None, overall_rank=None)

        page_records.append((year, race, lite_rec))

    
    ret_raw = pool.map(get_detail_record, page_records)
    
    ret = [rec for rec in ret_raw if rec.general.first != None or rec.general.last != None]       

    return ret



if __name__ == '__main__':        
    root = '../../wtc_race_data/v2'
    #get discontinued race
    # race = Race('americas', 'arizona', 'ironman', 'arizona')
    # year = 2011
    # year_result = get_race_year_brutal(race, year, 1,3000)
    # save_race(root, race, year_result, year, override=False)
    # sys.exit()

    race_list = []
    race_list += get_ironman_race()
        
    # race_list += get_ironman703_race()

    # create_data_folder('./Data', race_list)    
    race_list = [Race('asiapac', 'saipan', 'ironman-70.3', 'saipan')]
    for race in race_list:            
        years = (get_years(race))
        years = [2017]

        for year in years:
            year_result = get_race_year(race, year, check_exist={'root': root})
            save_race(root, race, year_result, year, override=True)

   