import sys
import os
from subprocess import call

if __name__ == "__main__":
    root = sys.argv[1]
    for d in os.walk(root, topdown=True):
        for f in d[2]:
            filename = os.path.join(d[0], f) 
            if filename[-5:] == '.json':
                print('import {0}'.format(filename))
                call(['mongoimport', '--db', 'wtc', '--collection', 'result_raw', filename ,'--jsonArray'])