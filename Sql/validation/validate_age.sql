SELECT *
FROM race
WHERE division NOT IN ('PRO', 'PC')
      AND (race.age_as_race < convert(substr(division, 1, 2), SIGNED INTEGER) - 1 OR
           race.age_as_race > CONVERT(substr(division, 4, 2), SIGNED INTEGER) + 1)



