INSERT INTO race
SELECT
  region,
  race,
  type,
  CONVERT(date, UNSIGNED INTEGER) DIV 10000                    AS year,
  STR_TO_DATE(date, '%Y%m%d')                                  AS date,
  CASE header_status
  WHEN '---'
    THEN 'FINISH'
  ELSE header_status END                                       AS status,
  name_gender                                                  AS gender,
  name_first                                                   AS first_name,
  name_last                                                    AS last_name,
  CASE general_age
  WHEN ''
    THEN NULL
  ELSE CONVERT(general_age, UNSIGNED INTEGER) + CONVERT((CONVERT(date, UNSIGNED INTEGER) DIV 10000), UNSIGNED INTEGER) -
       CONVERT(as_of_year, UNSIGNED INTEGER) END               AS age_as_race,
  CASE general_age
  WHEN ''
    THEN NULL
  ELSE CONVERT(as_of_year, UNSIGNED INTEGER) - general_age END AS birth_year,
  general_bib                                                  AS bib,

  CASE general_state
  WHEN '---'
    THEN NULL
  ELSE general_state END                                       AS state,

  general_country                                              AS country,
  general_division                                             AS division,
  CASE general_points
  WHEN ''
    THEN NULL
  ELSE CONVERT(general_points, UNSIGNED INTEGER) END           AS point,
  CASE general_profession
  WHEN '---'
    THEN NULL
  ELSE general_profession END                                  AS profession,

  CASE WHEN header_div_rank = ''
    THEN NULL
  ELSE convert(header_div_rank, UNSIGNED INTEGER) END          AS div_rank,
  CASE WHEN header_gender_rank = ''
    THEN NULL
  ELSE convert(header_gender_rank, UNSIGNED INTEGER) END       AS gender_rank,

  CASE WHEN header_overall_rank = ''
    THEN NULL
  ELSE convert(header_overall_rank, UNSIGNED INTEGER) END      AS overall_rank,

  CASE WHEN summary_swim = ''
    THEN NULL
  ELSE convert(summary_swim, TIME) END                         AS swim,
  CASE WHEN transition_t1 = ''
    THEN NULL
  ELSE convert(transition_t1, TIME) END                        AS t1,

  CASE WHEN summary_bike = ''
    THEN NULL
  ELSE convert(summary_bike, TIME) END                         AS bike,
  CASE WHEN transition_t2 = ''
    THEN NULL
  ELSE convert(transition_t2, TIME) END                        AS t2,

  CASE WHEN summary_run = ''
    THEN NULL
  ELSE convert(summary_run, TIME) END                          AS run,

  CASE WHEN summary_overall = ''
    THEN NULL
  ELSE convert(summary_overall, TIME) END                      AS overall,


  CASE WHEN swim_distance = ''
    THEN NULL
  ELSE swim_distance END                                       AS swim_distance,

  CASE WHEN swim_division_rank = ''
    THEN NULL
  ELSE convert(swim_division_rank, UNSIGNED INTEGER) END       AS swim_division_rank,

  CASE WHEN swim_gender_rank = ''
    THEN NULL
  ELSE convert(swim_gender_rank, UNSIGNED INTEGER) END         AS swim_gender_rank,

  CASE WHEN swim_overall_rank = ''
    THEN NULL
  ELSE convert(swim_overall_rank, UNSIGNED INTEGER) END        AS swim_overall_rank,

  CASE WHEN swim_pace = ''
    THEN NULL
  ELSE swim_pace END                                           AS swim_pace,

  CASE WHEN swim_race_time = ''
    THEN NULL
  ELSE convert(swim_race_time, TIME) END                       AS swim_race_time,

  CASE WHEN swim_split_time = ''
    THEN NULL
  ELSE convert(swim_split_time, TIME) END                      AS swim_split_time,


  CASE WHEN bike_distance = ''
    THEN NULL
  ELSE bike_distance END                                       AS bike_distance,

  CASE WHEN bike_division_rank = ''
    THEN NULL
  ELSE convert(bike_division_rank, UNSIGNED INTEGER) END       AS bike_division_rank,

  CASE WHEN bike_gender_rank = ''
    THEN NULL
  ELSE convert(bike_gender_rank, UNSIGNED INTEGER) END         AS bike_gender_rank,

  CASE WHEN bike_overall_rank = ''
    THEN NULL
  ELSE convert(bike_overall_rank, UNSIGNED INTEGER) END        AS bike_overall_rank,

  CASE WHEN bike_pace = ''
    THEN NULL
  ELSE bike_pace END                                           AS bike_pace,

  CASE WHEN bike_race_time = ''
    THEN NULL
  ELSE convert(bike_race_time, TIME) END                       AS bike_race_time,

  CASE WHEN bike_split_time = ''
    THEN NULL
  ELSE convert(bike_split_time, TIME) END                      AS bike_split_time,

  CASE WHEN run_distance = ''
    THEN NULL
  ELSE run_distance END                                        AS run_distance,

  CASE WHEN run_division_rank = ''
    THEN NULL
  ELSE convert(run_division_rank, UNSIGNED INTEGER) END        AS run_division_rank,

  CASE WHEN run_gender_rank = ''
    THEN NULL
  ELSE convert(run_gender_rank, UNSIGNED INTEGER) END          AS run_gender_rank,

  CASE WHEN run_overall_rank = ''
    THEN NULL
  ELSE convert(run_overall_rank, UNSIGNED INTEGER) END         AS run_overall_rank,

  CASE WHEN run_pace = ''
    THEN NULL
  ELSE run_pace END                                            AS run_pace,

  CASE WHEN run_race_time = ''
    THEN NULL
  ELSE convert(run_race_time, TIME) END                        AS run_race_time,

  CASE WHEN run_split_time = ''
    THEN NULL
  ELSE convert(run_split_time, TIME) END                       AS run_split_time
FROM race_staging