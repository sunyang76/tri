SELECT count(*)
FROM race
WHERE
  (race.age_as_race < convert(substr(division, 1, 2), SIGNED INTEGER) - 1 OR
   race.age_as_race > convert(substr(division, 4, 2), SIGNED INTEGER) + 1)
   AND division NOT IN ('PRO', 'PC')

order by age_as_race DESC
