import * as gulp from 'gulp';
import ProjectConfig from '../../config';

export = () => {
return gulp.src(ProjectConfig.FONTS_SRC)
      .pipe(gulp.dest(ProjectConfig.FONTS_DEST));
};