import { join } from 'path';

import { SeedConfig } from './seed.config';
import { ExtendPackages } from './seed.config.interfaces';

/**
 * This class extends the basic seed configuration, allowing for project specific overrides. A few examples can be found
 * below.
 */
export class ProjectConfig extends SeedConfig {

    PROJECT_TASKS_DIR = join(process.cwd(), this.TOOLS_DIR, 'tasks', 'project');
    FONTS_DEST = `${this.APP_DEST}/fonts`;
    FONTS_SRC = [
        'node_modules/bootstrap/dist/fonts/**',
        'node_modules/font-awesome/fonts/**'
    ];

  constructor() {
    super();
    // this.APP_TITLE = 'Put name of your app here';
    // this.GOOGLE_ANALYTICS_ID = 'Your site's ID';

        /* Enable typeless compiler runs (faster) between typed compiler runs. */
        // this.TYPED_COMPILE_INTERVAL = 5;

        // Add `NPM` third-party libraries to be injected/bundled.
        this.NPM_DEPENDENCIES = [
            ...this.NPM_DEPENDENCIES,
            { src: 'jquery/dist/jquery.min.js', inject: 'libs' },
            { src: 'tether/dist/js/tether.min.js', inject: 'libs' },
            { src: 'bootstrap/dist/js/bootstrap.min.js', inject: 'libs' },
            // { src: 'lodash/lodash.min.js', inject: 'libs' },
            { src: 'tether/dist/css/tether.min.css', inject: true },
            { src: 'bootstrap/dist/css/bootstrap.min.css', inject: true },
            { src: 'font-awesome/css/font-awesome.min.css', inject: true },
            { src: 'ag-grid/dist/styles/ag-grid.css', inject: true },
            { src: 'ag-grid/dist/styles/theme-fresh.css', inject: true },
            { src: 'ng2-dnd/bundles/style.css', inject: true },
            // {src: 'lodash/lodash.min.js', inject: 'libs'},
        ];

        // Add `local` third-party libraries to be injected/bundled.
        this.APP_ASSETS = [
            ...this.APP_ASSETS,
            // {src: `${this.APP_SRC}/your-path-to-lib/libs/jquery-ui.js`, inject: true, vendor: false}
            // {src: `${this.CSS_SRC}/path-to-lib/test-lib.css`, inject: true, vendor: false},
        ];

        // Add packages (e.g. ng2-translate)
        let additionalPackages: ExtendPackages[] = [{
            name: 'ag-grid/main',
            // Path to the package's bundle
            path: 'node_modules/ag-grid/main.js'
        }, {
            name: 'ag-grid-ng2/main',
            // Path to the package's bundle
            path: 'node_modules/ag-grid-ng2/main.js'
        }, {
            name: 'ng2-dnd',
            // Path to the package's bundle
            path: 'node_modules/ng2-dnd/bundles/index.umd.js'
        }, {
            name: 'lodash',
            // Path to the package's bundle
            path: 'node_modules/lodash/lodash.min.js'
        }, {
            name: 'angular2-google-maps/core',
            // Path to the package's bundle
            path: 'node_modules/angular2-google-maps/core/core.umd.js'
        }, {
            name: '@ng-bootstrap/ng-bootstrap',
            path: 'node_modules/@ng-bootstrap/ng-bootstrap/bundles/ng-bootstrap.js'
        }
            /*{
            name: 'ng2-translate',
            // Path to the package's bundle
            path: 'node_modules/ng2-translate/bundles/ng2-translate.umd.js'
            }*/

        ];

        this.addPackagesBundles(additionalPackages);

    /* Add proxy middlewar */
    // this.PROXY_MIDDLEWARE = [
    //   require('http-proxy-middleware')({ ws: false, target: 'http://localhost:3003' })
    // ];

    /* Add to or override NPM module configurations: */
    // this.PLUGIN_CONFIGS['browser-sync'] = { ghostMode: false };
  }

}
