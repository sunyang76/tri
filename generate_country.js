use wtc

var countries = db.name_lookup.aggregate([ 
    {'$group': {
        '_id': {
            'first': '$general.first',
            'last': '$general.last',
            'full': '$general.full',
            'gender': '$general.gender',
            'country': '$general.country'
        }
    }},
    {
        '$project': {
            '_id': 0,
            'first': '$_id.first',
            'last': '$_id.last',
            'full': '$_id.full',
            'gender': '$_id.gender',
            'country': '$_id.country'            
        }
    }
])
    
countries.forEach(function(item){
    db.country_lookup.save(item)
})
