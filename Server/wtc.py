# mongo.py
from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS, cross_origin
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
import re

app = Flask(__name__)
CORS(app)

app.config['MONGO_DBNAME'] = 'wtc'
app.config['MONGO_URI'] = 'mongodb://localhost:27017/wtc'

mongo = PyMongo(app)
MAX_RETURN = 5;

def get_sec(time_str):
    if time_str is None:
        return None;
    else:
        # h, m, s = time_str.split(':')
        # return int(h) * 3600 + int(m) * 60 + int(s)
        parts = time_str.split(':')
        length = len(parts)

        if length > 2:
            return int(parts[0]) * 3600 + int(parts[1]) * 60 + int(parts[2])
        if length > 1:
            return int(parts[0]) * 60 + int(parts[1])
        if length > 0:
            return int(parts[0])
        return None;

@app.route('/record/<string:id>', methods=['GET'])
def get_race_record(id):
    results = mongo.db.race_result
    output = []
    pipeline = []

    pipeline.append({'$match': {'_id' : ObjectId(id)}})

    for item in results.aggregate(pipeline):
        output.append({
            'id': id,
            'general': item['general'],
            'transition': item['transition'],
            'summary': item['summary'],
            'race': item['race'],
            'swim': item['swim'],
            'bike': item['bike'],
            'run': item['run']
        })

    return jsonify({'result' : output[0]})

@app.route('/athlete/<string:name>', methods=['GET'])
@app.route('/athlete/<string:name>/<int:limit>', methods=['GET'])
def search_athlete(name, limit = None):
    name_lookup = mongo.db.athlete_lookup

    output = []
    pipeline = []

    pipeline.append({'$match': {'full' : {'$regex': name , '$options': 'i'}}})

    pipeline.append(        
        {'$project': {
            '_id': 0,       
            'first': '$first',     
            'last': '$last',      
            'gender': '$gender',
            'country': '$country',      
            'length': {'$strLenBytes': '$full'}          
        }}
    )

    pipeline.append({ '$sort' : { 'length' : 1 } })
    if limit is not None:
        pipeline.append({'$limit': limit})
    else:    
        pipeline.append({'$limit': 50})

    for item in name_lookup.aggregate(pipeline):    
        output.append({           
            'first': item['first'],
            'last': item['last'], 
            'gender': item['gender'],
            'country': item['country']            
        })   
    
    return jsonify({'result' : output})


@app.route('/search/<string:gender>/<string:first>/<string:last>/<string:country>', methods=['GET'])
def search_by_name(gender, first, last, country):
    results = mongo.db.race_result

    output = []

    pipeline = []

    pipeline.append({'$match': { '$and': [{'general.gender': gender , 'general.first': first, 'general.last': last, 'general.country': country } ] }})

    pipeline.append(        
        {'$project': {
            'id': '$_id',       
            'bib': '$general.bib',     
            'first': '$general.first',
            'last': '$general.last',            
            'country': '$general.country',
            'state': '$general.state',
            'city': '$general.city',
            'gender': '$general.gender',
            'profession': '$general.profession',
            'division': '$general.division',
            'overall': '$summary.overall',
            'swim': '$summary.swim',
            'bike': '$summary.bike',
            'run': '$summary.run',
            'status': '$status',
            'year': '$year',
            'type': '$race.type',
            'region': '$race.region',
            'code': '$race.code'
        }}
    )
    
    for item in results.aggregate(pipeline):
        output.append({
            'id': str(item['id']),
            'year': item['year'],
            'type': item['type'],
            'region': item['region'],
            'code': item['code'],
            'bib': item['bib'],
            'first': item['first'], 
            'last': item['last'],
            'country': item['country'],
            'state': item['state'],
            'city': item['city'],
            'gender': item['gender'],
            'profession': item['profession'],
            'division': item['division'],
            'overall': item['overall'],
            'swim': item['swim'],
            'bike': item['bike'],
            'run': item['run'],
            'status': item['status']            
        })   

    return jsonify({'result' : output})

@app.route('/race/', methods=['GET'])
@app.route('/race/<int:year>', methods=['GET'])
def race(year=None):
    results = mongo.db.race_lookup

    output = []
    pipeline = []
    # whereClause = [{'year': race_year, 'race.region': race_region, 'race.code': race_code, 'race.type': race_type}]
    if year is not None:
        pipeline.append({
            '$match': {'year': year}
        })

    pipeline.append({
        '$project': {
            'id': '$_id',
            'year': '$year',
            'type': '$type',
            'region': '$region',
            'code': '$code',
            'name': '$name'
        }
    })

    for item in results.aggregate(pipeline):
        output.append({
            # 'id': item['id'],
            'year': item['year'],
            'type': item['type'],
            'region': item['region'],
            'code': item['code'],
            'name': item['name']
        })

    return jsonify({'result' : output})

@app.route('/race_result/<string:race_type>/<string:race_region>/<string:race_code>/<int:race_year>', methods=['GET'])
@app.route('/race_result/<string:race_type>/<string:race_region>/<string:race_code>/<int:race_year>/<int:limit>', methods=['GET'])
def race_result(race_type, race_region, race_code, race_year, limit = None):
    results = mongo.db.race_result

    output = []

    pipeline = []

    whereClause = [{'year': race_year, 'race.region': race_region, 'race.code': race_code, 'race.type': race_type}]

    pipeline.append({'$match': {'$and': whereClause}})

    pipeline.append(        
        {'$project': {
            'id': '$_id',       
            'bib': '$general.bib',     
            'first': '$general.first',
            'last': '$general.last',            
            'country': '$general.country',
            'state': '$general.state',
            'city': '$general.city',
            'gender': '$general.gender',
            'profession': '$general.profession',
            'division': '$general.division',
            'overall': '$summary.overall',
            'swim': '$summary.swim',
            'bike': '$summary.bike',
            'run': '$summary.run',
            'status': '$status',
            'year': '$year',
            'type': '$race.type',
            'region': '$race.region',
            'code': '$race.code',
            't1': '$transition.t1',
            't2': '$transition.t2',
        }}
    )
    
    if limit is not None:
        pipeline.append({'$limit': limit})

    for item in results.aggregate(pipeline):
        output.append({
            'id': str(item['id']),
            'year': item['year'],
            'type': item['type'],
            'region': item['region'],
            'code': item['code'],
            'bib': item['bib'],
            'first': item['first'], 
            'last': item['last'],
            'country': item['country'],
            'state': item['state'],
            'city': item['city'],
            'gender': item['gender'],
            'profession': item['profession'],
            'division': item['division'],
            'overall': item['overall'],
            'swim': item['swim'],
            'bike': item['bike'],
            'run': item['run'],
            'status': item['status'],
            't1': item.get('t1', None),
            't2': item.get('t2', None)         
        })    

        # output.append( {
        #     'id': str(item['_id']),
        #     'year': item['year'],
        #     'general': item['general'],
        #     'race': item['race'],
        #     'summary': item['summary'],
        #     'status': item['status'],
        #     'swim': item['swim'],
        #     'bike': item['bike'],
        #     'run': item['run'],
        #     'transition': item['transition'],
        # })


    return jsonify({'result' : output})

if __name__ == '__main__':
    app.run(debug=True)
